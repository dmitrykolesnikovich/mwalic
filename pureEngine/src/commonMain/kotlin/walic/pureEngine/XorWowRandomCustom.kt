package walic.pureEngine

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.random.Random


/**
 * Random number generator, using Marsaglia's "xorwow" algorithm
 *
 * Cycles after 2^192 - 2^32 repetitions.
 *
 * For more details, see Marsaglia, George (July 2003). "Xorshift RNGs". Journal of Statistical Software. 8 (14). doi:10.18637/jss.v008.i14
 *
 * Available at https://www.jstatsoft.org/v08/i14/paper
 *
 */
@Serializable
class XorWowRandomCustom
(
        private var x: Int,
        private var y: Int,
        private var z: Int,
        private var w: Int,
        private var v: Int,
        private var addend: Int
) : Random() {
    constructor(seed: Long) : this(seed.toInt(), seed.shr(32).toInt())
    constructor(seed1: Int, seed2: Int) :
            this(seed1, seed2, 0, 0, seed1.inv(), (seed1 shl 10) xor (seed2 ushr 4))

    init {
        require((x or y or z or w or v) != 0) { "Initial state must have at least one non-zero element." }
    }


    override fun nextInt(): Int {
//        println("${callCount++} ${Json.encodeToString(this)}")
        // Equivalent to the xorxow algorithm
        // From Marsaglia, G. 2003. Xorshift RNGs. J. Statis. Soft. 8, 14, p. 5
        var t = x
        t = t xor (t ushr 2)
        x = y
        y = z
        z = w
        val v0 = v
        w = v0
        t = (t xor (t shl 1)) xor v0 xor (v0 shl 4)
        v = t
        addend += 362437
//        println("NRGED ${t + addend}")
        return t + addend
    }

    override fun nextBits(bitCount: Int): Int =
            nextInt().ushr(32 - bitCount) and (-bitCount).shr(31)
}

