package walic.pureEngine

import kotlin.random.Random


enum class TargetMods {
    Any,
    Enemy,
    Ally;

    fun checkIfOk(playerId: Int, pickId: Int): Boolean {
        if (pickId != 0 && pickId != 1)
            return false
        return when (this) {
            Any -> true
            Enemy -> playerId != pickId
            Ally -> playerId == pickId
        }
    }
}


suspend fun State.draw(playerId: Int, amount: Int, filter: (Card) -> Boolean = { true }, realved: Boolean = false) {
    val acctualAmmont = steamAdapter.getInfo(playerId) { sih ->
        val ret = sih.getNTopCard(amount, filter)
        sih.cardInHand.addAll(ret)
        return@getInfo ret.size
    }
    val cardsDrawn = (0 until acctualAmmont).map { getUnknowCard() }
    players[playerId].knownHand.addAll(cardsDrawn)
    customPrint(State.PrintOption.Draw(playerId, cardsDrawn))
    if (realved) {
        var i = players[playerId].knownHand.size - 1
        repeat(acctualAmmont) {
            revalCard(playerId, TargetMods.Ally) { list, _ -> list.indices.last }
            i -= 1
        }
    }

}

enum class DrawNPickMMods {
    ToHand,
    ToHandGetRest,
    ToHandRestBootom,
    ToGame,
}


suspend fun State.drawNPick1(playerId: Int, m: Int, top: Boolean, mod: DrawNPickMMods, filter: (Card) -> Boolean = { true }): Card? =
    this.drawNPickM(playerId, m, 1, top, mod, filter).firstOrNull()


suspend fun State.drawNPickM(playerId: Int, n: Int, m: Int, top: Boolean, mod: DrawNPickMMods, filter: (Card) -> Boolean = { true }): List<Card> {
    var (toHand, toDeck) = steamAdapter.getInfoWithUserInput<List<Int>, Pair<List<Card?>, List<Card>>>(playerId) { eval, sih ->
        val cardsN = when {
            n == -1 -> sih.cardToDraw.filter(filter)
            top -> {
                val temp = sih.getNTopCard(n, filter);sih.cardToDraw += temp;temp
            }
            else -> sih.cardToDraw.filter(filter).pickUpToN(n, sih.random)
        }
        val pickCards = if (cardsN.size > m) {
            val temp = eval { it.pickFromList(cardsN, m) }.map { cardsN[it] }
            if (temp.size != m)
                throw WrongInputException("${temp.size}!=$m @${cardsN.size}")
            if (temp.distinct().size != temp.size)
                throw WrongInputException("Not all disticnt $temp")
            temp
        } else {
            cardsN.toMutableList()
        }
        sih.cardToDraw.removeAll { cardsN.contains(it) }
        val remaningCards = cardsN.filter { !pickCards.contains(it) }
        val cardToHand: List<Card> = when (mod) {
            DrawNPickMMods.ToHand, DrawNPickMMods.ToHandGetRest, DrawNPickMMods.ToHandRestBootom -> pickCards
            else -> listOf()
        }
        val cardToReturn = when (mod) {
            DrawNPickMMods.ToGame -> pickCards
            DrawNPickMMods.ToHandGetRest -> remaningCards
            else -> listOf()
        }
        sih.cardInHand.addAll(cardToHand)

        when (mod) {
            DrawNPickMMods.ToHand, DrawNPickMMods.ToGame -> sih.cardToDraw.addAll(remaningCards)
            DrawNPickMMods.ToHandRestBootom -> {
                sih.cardToDraw.addAll(remaningCards)
                remaningCards.forEach { sih.moveToBottom(it) }
            }
            DrawNPickMMods.ToHandGetRest -> {
            }
        }

        val cardToHandVisibale: List<Card?> = cardToHand.map { null }//mozna cos innego

        return@getInfoWithUserInput Pair(cardToHandVisibale, cardToReturn)
    }
    toHand = toHand.map { it ?: getUnknowCard() }
    customPrint(State.PrintOption.Draw(playerId, toHand))
    players[playerId].knownHand.addAll(toHand)
    toDeck.forEach { it.state = this }
    return toDeck
}


suspend fun State.summonRandom(playerId: Int, abstractCard: AbstractCard): Card? = summonRandom(playerId) { it.ac == abstractCard }

suspend fun State.summonRandom(playerId: Int, filter: (Card) -> Boolean = { true }): Card? {
    getRandomFromDeck(playerId, filter)?.apply {
        state = this@summonRandom
        playCardwithBattlecryAndEverything(playerId, AbstractCard.PlayMod.FromDeck)
        customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Summoned, this))
        return this
    }
    return null
}

suspend fun State.summon(playerId: Int, filter: (Card) -> Boolean): Card? {
    return drawNPick1(playerId,-1,false, DrawNPickMMods.ToGame,filter)?.apply { playCardwithBattlecryAndEverything(playerId, AbstractCard.PlayMod.FromDeck) }
}

suspend fun State.getRandomFromDeck(playerId: Int, filter: (Card) -> Boolean): Card? {
    return steamAdapter.getInfo(playerId) { sih -> return@getInfo sih.cardToDraw.removeRandom(sih.random, filter) }
}

suspend fun State.getTopFromDeck(playerId: Int, filter: (Card) -> Boolean): Card? {
    return steamAdapter.getInfo(playerId) { sih -> return@getInfo sih.getNTopCard(1, filter).elementAt(0) }
}


suspend fun State.summonAllCopies(playerId: Int, abstractCard: AbstractCard, location: CardLocation?) {
    val input = steamAdapter.getInfo(playerId) { sih ->
        val all = sih.cardToDraw.filter { it.ac == abstractCard }
        sih.cardToDraw.removeAll(all)
        return@getInfo all
    }

    for (c in input) {
        c.state = this
        customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Summoned, c))
        if (location == null) {
            c.playCardwithBattlecryAndEverything(playerId, AbstractCard.PlayMod.FromDeck)
        } else {
            putCard(c, location)

        }
    }
}


suspend fun State.forceToPlay(playerId: Int, filter: (Card) -> Boolean = { true }, randomPlace: Boolean = false, canLeaderAndPass: Boolean = false): Boolean {
    val (card, relesed) = pickFromOwnHand(playerId, filter, canLeaderAndPass = canLeaderAndPass, purpurs = "Play")
        ?: return false
    customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.PlayedFromHand, card))
    val fLocation = if (randomPlace) CardLocation(playerId, getRng().nextInt(0, 3)) else null
    card.playCardwithBattlecryAndEverything(playerId, if (relesed) AbstractCard.PlayMod.FromHandReveled else AbstractCard.PlayMod.FromHand, forceedLocation = fLocation)
    return true
}


suspend inline fun <reified A> State.actionOnDeck(playerId: Int, crossinline function: (MutableList<Card>, Random) -> A): A {
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.ActionOnDeck))
    return steamAdapter.getInfo(playerId) { function(it.cardToDraw, it.random) }
}

suspend inline fun <reified A> State.getInfoAboutInitialDeck(playerId: Int, crossinline function: (List<AbstractCard>, random: Random) -> A): A {
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.GetInfoAboutInitialDeck))
    return steamAdapter.getInfo(playerId) { function(it.initialDeck!!, it.random) }
}

suspend inline fun <reified A> State.getInfoAboutHand(playerId: Int, crossinline function: (List<Card>, random: Random) -> A): A {
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.GetInfoAboutHand))
    return steamAdapter.getInfo(playerId) { function(it.cardInHand, it.random) }
}


suspend fun <A> State.applyActionToReveladCard(playerId: Int, filter: (Card, Boolean) -> Boolean, action: (Card) -> A): A? {
    val possiblites = players.mapIndexed { index, player -> player.knownHand.filter { it.ac !is UnknowCard }.filter { filter(it, playerId != index) } }.flatten()
    if (possiblites.isEmpty())
        return null
    val id = steamAdapter.getInput(playerId) { it.pickFromList(possiblites) }[0]
    val card = possiblites[id]
    val player = players.indexOfFirst { it.knownHand.contains(card) }
    val index = players[player].knownHand.indexOf(card)
    steamAdapter.applyAction(player) { sih ->
        action(sih.cardInHand[index])
    }
    customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.ActionApplyedToReveldCard, card))
    return action(card)
}

suspend fun State.concalAnyAndApplyFunction(playerId: Int, ammount: Int, action: (Card, Boolean) -> Unit) {
    val possiblites = players.map { it.knownHand }.flatten().filter { it.ac !is UnknowCard }
    if (possiblites.isEmpty())
        return
    val cards = steamAdapter.getInput(playerId) { it.pickFromList(possiblites, canLess = true, n = ammount) }
    if (cards.size > ammount || cards.distinct().size != cards.size)
        throw WrongInputException("${cards.size}<=$ammount")
    cards.forEach { id ->
        val card = possiblites[id]
        val player = players.indexOfFirst { it.knownHand.contains(card) }
        val index = players[player].knownHand.indexOf(card)
        steamAdapter.applyAction(player) { sih ->
            action(sih.cardInHand[index], player != playerId)
        }
        val oldCard = players[player].knownHand[index]
        players[player].knownHand[index] = getUnknowCard()
        customPrint(State.PrintOption.Reveled(oldCard, players[player].knownHand[index]))
    }

}


suspend fun State.boostRandomInHand(playerId: Int, count: Int, amont: Int) {
    steamAdapter.getInfo(playerId) { sih ->
        val cards = sih.cardInHand.filter { !it.ist(Tag.SPECIAL) }.pickUpToN(count, sih.random)
        cards.forEach { it.currentStrength += amont }
        return@getInfo cards.map { sih.cardInHand.indexOf(it) }
    }.map { players[playerId].knownHand[it] }.filter { it.ac !is UnknowCard }.forEach {
        it.currentStrength += amont
        customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.BoostedInHand, it))
    }
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.BoostRandomInHand))
}

suspend fun State.boostAllInHand(playerId: Int, amount: Int, filter: (Card) -> Boolean = { true }) {
    steamAdapter.applyAction(playerId) { sih ->
        sih.cardInHand.filter(filter).forEach { it.currentStrength += amount }
    }
    players[playerId].knownHand.filter { it.ac !is UnknowCard }.filter(filter).forEach {
        it.currentStrength += amount
        customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.BoostedInHand, it))
    }
}

suspend fun State.boostAllInDeck(playerId: Int, amount: Int, filter: (Card) -> Boolean = { true }) {
    steamAdapter.applyAction(playerId) { sih ->
        sih.cardToDraw.filter(filter).forEach { it.currentStrength += amount }
    }
}

suspend fun State.boostAllInDeckAndHand(playerId: Int, amount: Int, filter: (Card) -> Boolean = { true }) {
    boostAllInDeck(playerId, amount, filter)
    boostAllInHand(playerId, amount, filter)
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.BoostAllInDeckAndHand))
}


//simple keywords


suspend fun State.duel(card: Card, traget: Card?, damageMult: Int = 1) {
    if (traget == null || traget.notOnBattlefiled())
        return
    customPrint(State.PrintOption.Dueal(card, traget))
    while (true) {
        if (traget.dealDamage(card.currentStrength * damageMult))
            break
        if (card.dealDamage(traget.currentStrength))
            break
    }
}


suspend fun State.drain(target: Card, reciver: Card, multi: Double = 1.0, atMost: Int = Int.MAX_VALUE) {
    customPrint(State.PrintOption.Drain(target, reciver))
    val damage = (target.currentStrength * multi).toInt().coerceAtMost(target.currentStrength - 1).coerceAtMost(atMost)
    target.dealDamage(damage)
    reciver.currentStrength += damage
}

suspend fun State.drainBoost(target: Card, reciver: Card, multi: Double = 1.0) {
    customPrint(State.PrintOption.Drain(target, reciver))
    val bostval = target.currentStrength - target.baseStrength
    target.currentStrength -= bostval
    reciver.currentStrength += (bostval * multi).toInt()
}


suspend fun State.consume(dealer: Card, target: Card, base: Boolean = false) {
    val howMuch = if (base) target.baseStrength else target.currentStrength
    if (!target.notOnBattlefiled())
        target.destroy()
    else
        target.banish(target.owner)
    dealer.boost(howMuch)
}


suspend fun State.applyWeather(location: CardLocation, weather: Weather?) {
    players[location.playerId].weathers[location.rowId] = weather
    customPrint(State.PrintOption.ApplyWether(location, weather))
}


suspend fun State.swap(playerId: Int, handFilter: (Card) -> Boolean = { true }, deckFilter: (Card) -> Boolean = { true }, ammount: Int = 1) {

    val (cardsRemoved, addedAmount) = steamAdapter.getInfoWithUserInput<List<Int>, Pair<List<Int>, Int>>(playerId) { eval, sih ->
        val hand = sih.cardInHand.filter(handFilter)
        customPrint(State.PrintOption.InfoForPlayer("Pick $ammount card from own hand to swap"))
        val cards = eval { it.pickFromList(hand, ammount, true) }.map { sih.cardInHand[it] }
        if (cards.size > ammount)
            throw WrongInputException("${cards.size}>$ammount")
        if (cards.distinct().size != cards.size)
            throw WrongInputException("Not all disticnt $cards")

        val oldCardIndices = cards.map { sih.cardInHand.indexOf(it) }
        val cardToAdd = sih.getNTopCard(cards.size, deckFilter)

        sih.cardInHand.removeAll(cards)
        sih.cardToDraw.addAll(cards)
        sih.cardInHand.addAll(cardToAdd)
        return@getInfoWithUserInput Pair(oldCardIndices, cardToAdd.size)
    }
    cardsRemoved.sorted().reversed().forEach {
        customPrint(State.PrintOption.AddToDeck(players[playerId].knownHand.removeAt(it), playerId))

    }

    val cards = (0 until addedAmount).map { getUnknowCard() }
    players[playerId].knownHand.addAll(cards)
    customPrint(State.PrintOption.Draw(playerId, cards, true))
}


fun State.isTruce(): Boolean {
    return players.none { it.didPass }
}


suspend fun State.revalCard(playerId: Int, whoes: TargetMods, how: ((List<Card>, Random) -> Int)? = null): Card? {
    val index: Int
    val ownerId: Int
    if (how != null) {
        val target = when (whoes) {
            TargetMods.Any -> throw WrongInputException("Shit is broken")
            TargetMods.Enemy -> 1 - playerId
            TargetMods.Ally -> playerId
        }
        val possibleIndicies = players[target].knownHand.mapIndexed { cardIndex, card -> if (card.ac is UnknowCard) cardIndex else null }.filterNotNull()
        index = steamAdapter.getInfo(target) { sih ->
            val possiblites = possibleIndicies.map { sih.cardInHand[it] }
            if (possiblites.isEmpty())
                return@getInfo -1
            val cardIndex = how(possiblites, sih.random)
            if (cardIndex == -1)
                return@getInfo -1
            return@getInfo sih.cardInHand.indexOf(possiblites[cardIndex])

        }
        ownerId = target

    } else {
        val possiblities = players.filterIndexed { localIndex, _ -> whoes.checkIfOk(playerId, localIndex) }.map { it.knownHand }.flatten().filter { it.ac is UnknowCard }
        if (possiblities.isEmpty())
            return null
        val card = possiblities[steamAdapter.getInput(playerId) { it.pickFromList(possiblities) }[0]]
        ownerId = players.indexOfFirst { it.knownHand.contains(card) }
        index = players[ownerId].knownHand.indexOf(card)
    }

    if (index < 0)
        return null
    val card = steamAdapter.getInfo(ownerId) {
        return@getInfo it.cardInHand[index]
    }
    val lastCard = players[ownerId].knownHand[index]
    players[ownerId].knownHand[index] = card
    card.state = this
    callForSingle("onRevelad", card) { it.onRevelad() }
    callForAll("onCardReveled") { it.onCardReveled(card, playerId) }

    customPrint(State.PrintOption.Reveled(lastCard, card))

    return card

}

suspend fun State.togleResilience(card: Card) {
    if (card.notOnBattlefiled())
        return
    customPrint(State.PrintOption.Simple(State.PrintOption.SingleMods.Resilance, card))
    card.resilience = !card.resilience
}

suspend fun State.spawnOneOF(playerId: Int, list: List<AbstractCard>): Card {
    val card = list[steamAdapter.getInput(playerId) { it.pickFromList(list) }[0]]
    return card.spawn(this, playerId)
}


suspend fun State.createCard(playerId: Int, ammount: Int = 3, filter: (AbstractCard) -> Boolean): Card? {
    val filtered = allNormalCards.filter(filter).filter { it.isFraction(players[playerId].fraction) }
    if (filtered.isEmpty())
        return null
    return steamAdapter.getInfoWithUserInput<List<Int>, AbstractCard>(playerId) { eval, sih ->
        val possibleCards = filtered.pickUpToN(ammount, sih.random)
        return@getInfoWithUserInput possibleCards[eval { it.pickFromList(possibleCards) }[0]]
    }.spawn(this, playerId)

}


suspend inline fun <reified A> State.applyActionAndMoveToTop(playerId: Int, crossinline action: (Card) -> A): A {
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.ApplyActionAndMoveToTop))
    return steamAdapter.getInfoWithUserInput<List<Int>, A>(playerId) { eval, sih ->
        val card = sih.cardToDraw[eval { it.pickFromList(sih.cardToDraw) }[0]]
        sih.moveToBottom(card)
        return@getInfoWithUserInput action(card)
    }
}

suspend inline fun <reified A> State.inHandValue(playerId: Int, crossinline filter: (Card) -> Boolean = { true }, crossinline function: (Card) -> A): A? {
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.InHandValue))
    return steamAdapter.getInfoWithUserInput<List<Int>, A?>(playerId) { eval, sih ->
        val cards = sih.cardInHand.filter(filter)
        if (cards.isEmpty())
            return@getInfoWithUserInput null
        val res = eval { it.pickFromList(cards) }[0]
        return@getInfoWithUserInput function(cards[res])
    }
}


suspend fun State.createCardFromEnemyStartingDeck(playerId: Int, ammount: Int = 3, filter: (AbstractCard) -> Boolean): AbstractCard? {
    val target = 1 - playerId
    val cards = getInfoAboutInitialDeck(target) { list, random ->
        list.filter(filter).pickUpToN(ammount, random)
    }
    if (cards.isEmpty())
        return null
    customPrint(State.PrintOption.Simpler(State.PrintOption.SimplerMods.CreateCardFromEnemyStartingDeck))
    return cards[steamAdapter.getInput(playerId) { it.pickFromList(cards) }[0]]


}

suspend fun State.boostAllWhereeverTheyAre(playerId: Int, filter: (Card) -> Boolean, ammount: Int) {
    getAlCardsOnBattleFiled(playerId).filter(filter).forEach { it.boost(ammount) }
    boostAllInDeckAndHand(playerId, ammount, filter)
}

suspend fun State.discardFromDeck(playerId: Int, filter: (Card) -> Boolean): Card? {
    val card = steamAdapter.getInfo(playerId) {
        val possiblites = it.cardToDraw.filter(filter)
        val card = possiblites.randomOrNull(it.random)
        it.cardToDraw.remove(card)
        return@getInfo card
    }
    card?.state = this
    card?.discard(playerId)
    return card
}



