package walic.pureEngine


import kotlin.random.Random


fun <A> List<A>.pickUpToN(n: Int, rng: Random): List<A> {
    if (this.size <= n)
        return this
    val ret = mutableListOf<A>()
    val remaing = this.toMutableList()
    repeat(n) {
        ret.add(remaing.removeAt(rng.nextInt(0, remaing.size)))
    }
    return ret
}

fun <A> MutableList<A>.switch(a: A): Boolean {
    return if (contains(a)) {
        remove(a)
        true
    } else {
        add(a)
        false
    }
}

fun <A> MutableList<A>.removeUpToN(n: Int, rng: Random): MutableList<A> {
    var toRemove = n
    val ret = mutableListOf<A>()
    while (toRemove > 0 && this.isNotEmpty()) {
        ret.add(this.removeAt(rng.nextInt(this.size)))
        toRemove--
    }
    return ret
}

fun <A> List<A>.allMin(minBy: (A) -> Int): List<A> {
    val minVal = minBy(minByOrNull(minBy) ?: return emptyList())
    return this.filter { minBy(it) == minVal }
}

inline fun <A> List<A>.allMax(crossinline maxBy: (A) -> Int) = allMin { -maxBy(it) }

fun <A> MutableList<A>.removeRandom(randomGen: Random, filter: (A) -> Boolean): A? {
    val matching = this.filter(filter)
    if (matching.isEmpty())
        return null
    val rand = matching.random(randomGen)
    this.remove(rand)
    return rand
}


class WrongInputException(override var message: String) : Exception() {

}


fun ByteArray.string(): String {
    return this.joinToString { it.toString() }
}


lateinit var allPossibleCards: List<AbstractCard>
lateinit var allLeaders: List<AbstractCard>
lateinit var allNormalCards: List<AbstractCard>
lateinit var allSpawner: List<AbstractCard>


fun getCard(name: String): AbstractCard {
    return (allPossibleCards + listOf(UnknowCard)).firstOrNull { it.name == name }
        ?: throw WrongInputException(name)
}

val camelRegex = "(?<=[a-zA-Z])[A-Z]".toRegex()


// String extensions
fun String.camelToHumanRedable(): String {
    return camelRegex.replace(this) {
        " ${it.value}"
    }.toLowerCase()
}