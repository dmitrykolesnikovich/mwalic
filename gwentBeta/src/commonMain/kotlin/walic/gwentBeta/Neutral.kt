package walic.gwentBeta

import walic.pureEngine.*


val allNeutralCards = listOf(
    Aguara,
    Caretaker,
    ColossalIfrit,
    Dudu,
    Eskel,
    EskelPathfinder,
    FrancisBedlam,
    Renew,
    Scorch,
    TaintedAle,
    TrialoftheGrasses,
    TrissMerigold,
    Vesemir,
    VigosMuzzle,
    CommandersHorn,
    CyprianWiley,
    AlzursThunder,
    ArachasVenom,
    DimeritiumShackles,
    Epidemic,
    Lacerate,
    MahakamAle,
    PeasantMilitia,
    PetrisPhilter,
    StammelfordsTremors,
    Swallow,
    BloodcurdlingRoar,
    Mardroeme,
    Overdose,
    Warcry,
    GeraltAard,
    GeraltofRivia,
    GermainPiquant,
    IrisCompanions,
    Lambert,
    LambertSwordmaster,
    Mandrake,
    ManticoreVenom,
    MerigoldsHailstorm,
    Myrgtabrakke,
    Necromancy,
    Nivellen,
    Phoenix,
    Regis,
    RaghNarRoog,
    TorrentialRain,
    WyvernScaleShield,
    AguaraTrueForm,
    Avallach,
    MarchingOrders,
    DimeritiumBomb,
    Doppler,
    ImpenetrableFog,
    MastercraftedSpear,
    Watchman,
    OlgierdvonEverec,
    IrisvonEverec,
    HanmarvynsBlueDream,
    GeraltIgni,
    BekkersRockslide,
    Ocvist,
    CiriDash,
    CiriNova,
    PrimordialDao,
    Johnny,
    KorathiHeatwave,
    Roach,
    RoyalDecree,
    TheLastWish,
    Vaedermakar,
    VesemirMentor,
    CarloVarese,
    AdrenalineRush,
    AvallachSage,
    RegisHigherVampire,
    Decoy,
    AleoftheAncestors,
    DandelionPoet,
    Garrison,
    TrissButterflies,
    YenneferConjurer,
    ZoltanScoundrel,
    CrowsEye,
    BekkersDarkMirror,
    SkelligeStorm,
    DorregarayofVole,
    SummoningCircle,
    Villentretenmerth,
    ArtefactCompression,
    WhiteFrost,
    Sihil,
    BlackBlood,
    YenneferofVengerberg,
    Operator,
    Wolfsbane,
    Sarah,
    ClearSkies,
    BitingFrost,
    Thunderbolt
)

private object DandelionPoet : AbstractCard() {
    override val defaultStrenght = 4
    override val tags: List<Tag> = listOf(Tag.Gold)
    override val description = "Deploy: Draw a card, then play a card."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.draw(playerId, 1)
        state.forceToPlay(playerId)
    }

}

private object Aguara : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Relict, Tag.Cursed, Tag.Gold)
    override val description = """Choose Two:
Boost the Lowest ally by 5
Boost a random unit in your hand by 5
Deal 5 damage to the Highest enemy
Charm a random enemy Elf with 5 power or less.
"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val picks = state.pickNofM(playerId, 2, description.split("\n").drop(1))
        if (picks.contains(0))
            state.players[playerId].rows.flatten().minByOrNull { it.currentStrength }?.boost(5)
        if (picks.contains(1))
            state.boostRandomInHand(playerId, 1, 5)
        if (picks.contains(2))
            state.players[1 - playerId].rows.flatten().maxByOrNull { it.currentStrength }?.dealDamage(5)
        if (picks.contains(3)) {
            val elfs = state.players[1 - playerId].rows.flatten().filter { it.tags.contains(Tag.Elf) }.filter { it.currentStrength <= 5 }
            if (elfs.isNotEmpty())
                elfs[state.getRng().nextInt(0, elfs.size)].charm()
        }


    }
}

private object AguaraTrueForm : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Relict, Tag.Cursed, Tag.Gold)
    override val description = "Create a Bronze or Silver Spell"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.Spell) }
    }
}


private object AleoftheAncestors : AbstractCard() {

    override var defaultStrenght = 10
    override val tags = listOf(Tag.Gold)
    override val description = "Apply a Golden Froth to the row -Boost all units by 2"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyWeather(thisCard.location(), GoldenFroth())
    }
}


private object Avallach : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Elf, Tag.Mage, Tag.Doomed, Tag.Gold)
    override val description = "Truce: Each player draws 2 cards"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.isTruce()) {
            state.draw(0, 2)
            state.draw(1, 2)
        }
    }
}


private object AvallachSage : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Elf, Tag.Mage, Tag.Gold)
    override val description = "Spawn a default copy of a random Gold or Silver unit from your opponent's starting deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.createCardFromEnemyStartingDeck(playerId, 1) { (it.ist(Tag.Gold) || it.ist(Tag.Silver)) && !it.ist(Tag.SPECIAL) }
            ?: return
        card.spawn(state, playerId)
    }
}


private object Caretaker : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Relict, Tag.Doomed, Tag.Gold)
    override val description = "Resurrect a Bronze or Silver unit from your opponent's graveyard"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId, TargetMods.Enemy) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && !it.ist(Tag.SPECIAL) }
            ?: return
        card.resurrect(playerId)
    }
}


private object Ciri : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Cintra, Tag.Witcher, Tag.Gold)
    override val description = "Whenever you lose a round, return this unit to your hand from graviard 2 Armor."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(2)
    }

    override suspend fun onStartOfRoundInGraviard(state: State, playerId: Int, thisCard: Card) {

    }
}


private object CiriDash : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Cintra, Tag.Witcher, Tag.Gold)
    override val description = "Whenever this unit enters the graveyard, return it to your deck and Strengthen it by 3"
    override suspend fun onPutInGraviard(state: State, thisCard: Card) {
        thisCard.strengthen(3)
        thisCard.addToDeck(thisCard.owner)
    }
}


private object CiriNova : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Cintra, Tag.Witcher, Tag.Doomed, Tag.Gold)
    override val description = "If you have exactly 2 copies of each Bronze card in your starting deck, set base power to 22"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val all2 = state.getInfoAboutInitialDeck(playerId) { list, _ ->
            list.groupBy { it.name }.filter { it.value[0].ist(Tag.Bronze) }.all { it.value.size == 2 }.toString()
        }.toBoolean()
        if (all2)
            thisCard.setBaseStreanght(22)
    }
}


private object ColossalIfrit : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Construct, Tag.Silver)
    override val description = """Spawn 3 Lesser Ifrits to the right of this unit.
Lesser Ifrit: 1 Strength, Construct, Token. Deal 1 damage to a random enemy"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) { LesserIfrit.spawn(state, playerId, forceedLocation = thisCard.location().right()) }
    }
}

object LesserIfrit : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Construct, Tag.TOKEN)
    override val description = "Deal 1 damage to a random enemy"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getRandomCardOnBattlefiled(playerId, mEnemy)?.dealDamage(1)
    }
}


private object Dudu : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Silver)
    override val description = "Set strength to that of an opposing non-Gold unit"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId)
        if (card != null)
            thisCard.setBaseStreanght(card.baseStrength)

    }
}


private object Eskel : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Witcher, Tag.Silver)
    override val description = "Summon Vesemir and Lambert"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Vesemir)
        state.summonRandom(playerId, Lambert)
    }
}


private object EskelPathfinder : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "Destroy a Bronze or Silver enemy that is not boosted."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId) { it.currentStrength <= it.baseStrength }?.destroy()
    }
}


private object FrancisBedlam : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Support, Tag.Silver)
    override val description = "If losing, Strengthen self up to a maximum of 15 until scores are tied"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val myStrenght = state.players[playerId].getStrenght()
        val enemySrenght = state.players[1 - playerId].getStrenght()
        if (myStrenght < enemySrenght)
            thisCard.boost(minOf(15, enemySrenght - myStrenght))
    }
}


private object Garrison : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override val description = "Create a Bronze or Silver unit from your opponent's starting deck and boost it by 2."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCardFromEnemyStartingDeck(playerId) { it.ist(Tag.Bronze) || it.ist(Tag.Silver) }?.spawn(state, playerId)?.boost(2)
    }
}


private object GaunterODimm : AbstractCard() {
    //TODO funck him
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Relict, Tag.Gold)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>6 Strength</u>, Relict, Gold, Legendary.
*<u>Gamble with Gaunter: Guess the power of the card he's picked to play it</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        //>6 <=5  dobrana to grasz
    }
}


private object GeraltofRivia : AbstractCard() {
    override var defaultStrenght = 15
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "No ability"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object GeraltAard : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "Choose 3 Enemies, push them to the row above them (if possible) and Damage them 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            val card = state.pickCardOnBattleFiled(playerId)
            if (card != null) {
                val cLane = card.location().rowId
                if (cLane != 2)
                    card.move(cLane + 1)
                card.dealDamage(3)
            }
        }
    }
}


private object GeraltIgni : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "Destroy the Highest units on an enemy row if that row has a total of 25 or more"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val rowId = state.pickRow(playerId, mEnemy)
        val row = state.getRowByLocation(rowId)
        if (row.sumBy { it.currentStrength } >= 25)
            row.allMax { it.currentStrength }.forEach { it.destroy() }

    }
}


private object GermainPiquant : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Officer, Tag.Silver)
    override val description = "Spawn 2 Cows on each side of this unit"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        Cow.spawn(state, playerId, location.right())
        Cow.spawn(state, playerId, location)
    }


}

object Cow : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Beast, Tag.TOKEN, Tag.Silver)
    override val description = "Token"

}

private object HanmarvynsBlueDream : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Gold)
    override val description = "Spawn a default copy of a non-Leader Gold unit from your opponent's graveyard and boost it by 2"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Enemy) { it.ist(Tag.Gold) && !it.ist(Tag.Leader) && !it.ist(Tag.SPECIAL) }?.ac?.spawn(state, playerId)?.boost(2)
    }
}


private object IrisCompanions : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Relict, Tag.Silver)
    override val description = "Move a card from your deck to your hand, then Discard a random card"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPick1(playerId, -1, false, DrawNPickMMods.ToHand)
        state.pickFromOwnHand(playerId, random = true)?.first?.discard(playerId) ?: return
    }
}


private object IrisvonEverec : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Redania, Tag.Cursed, Tag.Silver, Tag.SPY)
    override val description = "Spying. Deathwish: Boost 5 random units on the opposite side by 5."
    override suspend fun onDeath(state: State, thisCard: Card) {
        val location = thisCard.location()
        state.getAlCardsOnBattleFiled(1 - location.playerId).pickUpToN(5, state.getRng()).forEach {
            it.boost(5)
        }

    }
}


private object Johnny : AbstractCard() {
    override var defaultStrenght = 9

    override val tags = listOf(Tag.Relict, Tag.Silver)
    override val description = "Discard a card. Then make a copy of a card of the same color from your opponent's starting deck in your hand."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromOwnHand(playerId, purpurs = "Discard")?.first ?: return
        card.discard(playerId)
        val color = card.color()
        Card(state.createCardFromEnemyStartingDeck(playerId, 1) { it.ist(color) }
            ?: return, state.getNextUUID(), state).addToHand(playerId)

    }
}


private object KorathiHeatwave : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Gold)
    override val description = "Apply a Hazard to each enemy row that deals 2 damage to the Lowest unit on turn start."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        (0..2).forEach {
            state.applyWeather(CardLocation(1 - playerId, it, 0), KorathiHeatwaveWeather())
        }

    }
}


private object Lambert : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Witcher, Tag.Silver)
    override val description = "Summon Eskel and Vesemir"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Eskel)
        state.summonRandom(playerId, Vesemir)
    }
}


private object LambertSwordmaster : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "Deal 4 damage to all copies of an enemy unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId)
        if (card != null)
            state.players[1 - playerId].rows.flatten().filter { it.name == card.name }.forEach { it.dealDamage(4) }
    }
}


private object Mandrake : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Silver, Tag.Alchemy)
    override val description = """Choose One:
Heal a unit and Strengthen it by 6
Reset a unit and Weaken it by 6"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val i = state.pickNofM(playerId, 1, description.split("\n").drop(1))
        val card = state.pickCardOnBattleFiled(playerId, mAny)
        if (card != null)
            if (i[0] == 0) {
                card.heal()
                card.strengthen(6)
            } else {
                card.reset()
                card.weaken(6)
            }
    }
}


private object ManticoreVenom : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Silver)
    override val description = "Damage a unit by 13"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(13)
    }
}


private object MarchingOrders : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override val description = "Boost the Lowest Bronze or Silver Unit in your Deck by 2 and play it"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val lowest = state.actionOnDeck(playerId) { list, random ->
            list.filter { !it.ist(Tag.SPECIAL) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }.allMin { it.currentStrength }.randomOrNull(random)?.uuid
        }
        if (lowest != null) {
            val card = state.summonRandom(playerId) { it.uuid == lowest }!!
            card.boost(2)
        }

    }
}


private object MerigoldsHailstorm : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Silver)
    override val description = "Halve the power of all Bronze and Silver units on a row"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAny)
        state.getRowByLocation(row).filter { it.tags.contains(Tag.Silver) || it.tags.contains(Tag.Bronze) }.forEach {
            it.weaken(it.baseStrength / 2)
        }
    }
}


private object Myrgtabrakke : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Draconid, Tag.Silver)
    override val description = "Deal 2 damage 3 times"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            state.pickCardOnBattleFiled(playerId)?.dealDamage(2)
        }
    }
}


private object Necromancy : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Silver)
    override val description = "Banish a Bronze or Silver unit from either graveyard, then boost an ally by its power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val accu = listOf(0, 1).sumBy { p ->
            val card = state.pickFromGraviard(playerId, if (p == playerId) TargetMods.Ally else TargetMods.Enemy) { !it.ist(Tag.SPECIAL) }
            if (card != null) {
                state.players[p].graveyard.remove(card)
                card.banish(playerId)
                card.baseStrength
            } else 0
        }
        state.pickCardOnBattleFiled(playerId)?.boost(accu)
    }
}


private object Nivellen : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Cursed, Tag.Silver)
    override val description = "Move all units on a row to random rows"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAny)
        state.getRowByLocation(row).toList().forEach {
            it.move(state.getRng().nextInt(0, 3))
        }
    }
}


private object Ocvist : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Draconid, Tag.Silver, Tag.SINGLEUSE)
    override val description = "Single-Use. After 4 turns, deal 1 damage to all enemies, then return to your hand on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 4
    }

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter--
        if (thisCard.counter == 0) {
            state.getAlCardsOnBattleFiled(1 - playerId).forEach { it.dealDamage(1) }
            thisCard.addToDeck(playerId)
        }
    }
}


private object OlgierdvonEverec : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Redania, Tag.Cursed, Tag.Silver)
    override val description = "Deathwish: Resurrect this unit on a random row"
    override suspend fun onDeath(state: State, thisCard: Card) {
        thisCard.resurrect(thisCard.owner, true)
    }
}


private object Operator : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Mage, Tag.Silver, Tag.SINGLEUSE)
    override val description = "Truce: Make a default copy of a Bronze unit in your hand for both players. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromOwnHand(playerId, filter = { it.ist(Tag.Bronze) }, purpurs = " Make a default copy ")?.first
            ?: return
        card.defaultCopy(state.getNextUUID()).addToHand(playerId)
        card.defaultCopy(state.getNextUUID()).addToHand(1 - playerId)

    }
}


private object Phoenix : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Draconid, Tag.Doomed, Tag.Gold)
    override val description = "Resurrect a Bronze or Silver Draconid"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId) { it.ist(Tag.Draconid) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
            ?: return
        card.resurrect(playerId)
    }
}


private object PrimordialDao : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Construct, Tag.Bronze, Tag.Construct)
    override val description = "Deathwish: Spawn 2 Lesser Dao.Lesser D'ao: 4 Strength, Token"


    override suspend fun onDeath(state: State, thisCard: Card) {
        LesserDao.spawn(state, thisCard.owner, forceedLocation = thisCard.location())
        LesserDao.spawn(state, thisCard.owner, forceedLocation = thisCard.location())
    }
}

object LesserDao : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Construct, Tag.TOKEN)
    override val description = "Token"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}

private object RaghNarRoog : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Spell, Tag.Gold)
    override val description = "Apply a Hazard to each enemy row that deals 2 damage to the Highest unit on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        (0..2).forEach { state.applyWeather(CardLocation(1 - playerId, it, 0), RaghNarRoogWeather()) }
    }
}


private object Regis : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Vampire, Tag.Gold)
    override val description = "Drain all boosts from a unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny) { it.currentStrength > it.baseStrength }
        if (card != null) {
            thisCard.boost(card.currentStrength - card.baseStrength)
            card.weaken(card.currentStrength - card.baseStrength)
        }

    }
}


private object RegisHigherVampire : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Vampire, Tag.Gold)
    override val description = "Look at 3 random Bronze Units from your opponent's Deck. Consume 1, Boost self by <u>base Power</u> of consumed Unit, then shuffle the others back."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val cards = (0..3).map { state.getRandomFromDeck(1 - playerId) { it.ist(Tag.Bronze) } }.filterNotNull()
        if (cards.isEmpty())
            return
        cards.forEach { it.state = state }
        val card = state.pickOneOfCard(playerId, cards)
        thisCard.boost(card.baseStrength)
        card.discard(1 - playerId)
        cards.filter { it != card }.forEach { it.addToDeck(1 - playerId) }
    }
}


private object Renew : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Gold)
    override val description = "Resurrect a Gold Unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId) { it.ist(Tag.Gold) && !it.ist(Tag.SPECIAL) } ?: return
        card.resurrect(playerId)
    }
}


private object Roach : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Beast, Tag.Silver)
    override val description = "Whenever you play a Gold unit from your hand, Summon this unit. "
}

object RoachSprawner : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.SPAWNER)
    override val description = "Sprawner"

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.ist(Tag.Gold))
            state.summonRandom(card.owner, Roach)
    }
}


private object RoyalDecree : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Tactic, Tag.Gold)
    override val description = "Play a Gold <u>unit</u> from your deck and boost it by 2."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it.ist(Tag.Gold) && !it.ist(Tag.SPECIAL) }?.boost(2)
    }
}


private object SaesenthessisBlaze : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Aedirn, Tag.Draconid, Tag.Gold)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Saesenthessis: Blaze</u>, Aedirn, Draconid, Gold, Legendary.
*Banish your hand and draw that many cards.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object Sarah : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Relict, Tag.Silver)
    override val description = "Swap a card for one of the same color"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val colors = listOf(Tag.Gold, Tag.Silver, Tag.Bronze)
        val color = colors[state.pickNofM(playerId, 1, colors.map { it.toString() })[0]]
        state.swap(playerId, { it.ist(color) }, { it.ist(color) })
    }
}


object Scorch : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = "Destroy all the Highest Units"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val maxCard = state.getAlCardsOnBattleFiled().maxByOrNull { it.currentStrength }
        if (maxCard != null)
            state.players.map { it.rows }.flatten().flatten().filter { it.currentStrength == maxCard.currentStrength }
                .forEach { it.destroy() }


    }
}


private object ShupesDayOff : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Gold)
    override val description = "If your starting deck has no duplicates, send Shupe on an adventure."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (!state.getInfoAboutInitialDeck(playerId) { list, _ ->
                list.size == list.map { it.name }.distinct().size
            })
            return
        state.spawnOneOF(playerId, listOf(SteaShupeHunter, ShupeKnight, ShupeMage))


    }

}


object SteaShupeHunter : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Ogroid, Tag.TOKEN, Tag.Gold)
    override val description = """Send Shupe to the forests of Dol Blathanna.
Deal 15 damage.
Deal 2 damage to a random enemy.Repeat 8 times.
Replay a Bronze or Silver ally and boost it by 5.
Clear all Hazards from your side and boost allies by 1.
Play a Bronze or Silver unit from your deck"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object ShupeKnight : AbstractCard() {
    override var defaultStrenght = 12
    override val tags = listOf(Tag.Ogroid, Tag.TOKEN, Tag.Gold)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*12 Strength, Ogroid, Token, Gold, Legendary.
*Send Shupe to the Imperial Court Military Academy. 01. Strengthen self to 25. 02. Resilient, 03. Duel an enemy. 04. Reset a unit. 05. Destroy enemies with less than 4 power. <u>Added to the game</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object ShupeMage : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Ogroid, Tag.TOKEN, Tag.Gold)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*4 Strength, Ogroid, Token, Gold, Legendary.
*Send Shupe to the Ban Ard Academy for Boys. 01. Draw a card. 02. Charm a random enemy. 03. Spawn random Hazards on all enemy rows. 04. Deal 10 damage to an enemy and 5 to any adjacent to it. 05. Play a Bronze or Silver special card from your deck. <u>Added to the game</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object Sihil : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Gold)
    override val description = """*Choose one: 
        Deal 3 damage to all enemies with odd power;
        Deal 3 damage to all enemies with even power;
        Play a random Bronze or Silver unit from your Deck*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0]) {
            0 -> state.getAlCardsOnBattleFiled(1 - playerId).filter { it.currentStrength % 2 == 1 }.forEach { it.dealDamage(3) }
            1 -> state.getAlCardsOnBattleFiled(1 - playerId).filter { it.currentStrength % 2 == 0 }.forEach { it.dealDamage(3) }
            2 -> state.summonRandom(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && !it.ist(Tag.SPECIAL) }
        }
    }
}


private object SkelligeStorm : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Silver)
    override val description = "Apply a Hazard to an enemy row that deals 2, 1 and 1 damage to the leftmost units on the row on turn start."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mEnemy)
        state.applyWeather(row, SkelligeStormWether())
    }
}


private object SummoningCircle : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Silver)
    override val description = "Spawn a default copy of the last Bronze or Silver <u>non-Agent</u> unit placed on the board."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.cardPlayedHistory.map { it.first }.filter { !it.ist(Tag.SPY) && !it.ist(Tag.SPECIAL) }.lastOrNull { it.ist(Tag.Silver) || it.ist(Tag.Gold) }?.spawn(state, playerId)
    }
}


private object TaintedAle : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Silver, Tag.Alchemy)
    override val description = "Deal 6 damage to the Highest enemy on each row"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[1 - playerId].rows.forEach { list ->
            val highest = list.maxByOrNull { it.baseStrength }
            if (highest != null)
                list.filter { it.baseStrength == highest.baseStrength }.forEach { it.dealDamage(6) }
        }
    }
}


private object TheLastWish : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = "Draw your top 2 <u>cards</u>. Play 1 and shuffle the other back."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(playerId, 2, true, DrawNPickMMods.ToGame) ?: return
        card.playCardwithBattlecryAndEverything(playerId, PlayMod.FromDeck)
    }
}


private object TrialoftheGrasses : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Gold)
    override val description = "Deal 10 damage to a unit, unless it's a Witcher. If it survives, boost it to 25 power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny)
        if (card != null) {
            if (card.tags.contains(Tag.Witcher) || !card.dealDamage(10))
                card.boost(25)
        }
    }
}


private object TrissMerigold : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Temeria, Tag.Mage, Tag.Gold)
    override val description = "Deal 5 damage"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.dealDamage(5)
    }
}


private object TrissButterflies : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Temeria, Tag.Mage, Tag.Gold)
    override val description = "Boost the Lowest allies by 1 on turn end"

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).allMin { it.currentStrength }.forEach { it.boost(1) }
    }
}


private object TrissTelekinesis : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Temeria, Tag.Mage, Tag.Gold)
    override val description = """Patch|Feb 28, 2018|0.9.22 Bear Season.
*6 Strength, Temeria, Mage, Gold, Legendary.
*Create a Bronze special card from either player's starting deck. <u>Added to the game</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object Vaedermakar : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Mage, Tag.Silver)
    override val description = "Spawn Biting Frost, Impenetrable Fog or Alzur's Thunder."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(BitingFrost, ImpenetrableFog, AlzursThunder))
    }
}


private object Vesemir : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Witcher, Tag.Silver)
    override val description = "Summon Eskel and Lambert"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Eskel)
        state.summonRandom(playerId, Lambert)
    }
}


private object VesemirMentor : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Witcher, Tag.Gold)
    override val description = "Play a Bronze or Silver Alchemy card from your deck"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { it.ist(Tag.Alchemy) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
    }
}


private object VigosMuzzle : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Gold)
    override val description = "Charm a Bronze or Silver enemy with 8 power or less"


    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mEnemy) { it.currentStrength <= 8 }?.charm()

    }
}


private object Villentretenmerth : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Draconid, Tag.Gold)
    override val description = "onTurnsEnd, After 3 turns, destroy the Highest units. 3 Armor."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(3)
        thisCard.counter = 3
    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter--
        if (thisCard.counter == 0) {
            state.getAlCardsOnBattleFiled().allMax { it.baseStrength }.forEach { it.destroy() }
        }
    }
}


private object WhiteFrost : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Silver)
    override val description = "Apply a Frost <u>Hazard</u> to 2 adjacent opposing rows"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mask = RowMask(ally = listOf(false, false, false), enemy = listOf(true, true, false)))
        state.applyWeather(row, FrostWether())
        row.rowId++
        state.applyWeather(row, FrostWether())
    }
}


private object Wolfsbane : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Organic, Tag.Gold)
    override val description = "While in your Graveyard, after 3 turns, at the start of your turn, Boost the Lowest Ally by 6 and Damage the Highest Enemy by 6."


    override suspend fun onPutInGraviard(state: State, thisCard: Card) {
        thisCard.counter = 3
    }

    override suspend fun onTurnStartInGraviard(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter--
        if (thisCard.counter == 0) {
            state.getAlCardsOnBattleFiled(playerId).allMin { it.baseStrength }.randomOrNull(state.getRng())?.boost(6)
            state.getAlCardsOnBattleFiled(1 - playerId).allMax { it.baseStrength }.randomOrNull(state.getRng())?.dealDamage(6)
        }
    }

}


private object YenneferofVengerberg : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Mage, Tag.Aedirn, Tag.Gold, Tag.Beast, Tag.Cursed)
    override val description = """Choose One:
Spawn a Unicorn that boosts all units by 2;Unicorn: 1 Strength
Spawn a Chironex that deals 2 damage to all units;Chironex: 4 Strength,
*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(Chironex, Unicorn))
    }


}

object Chironex : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Cursed, Tag.TOKEN)
    override val description = "Boosts all units by 2"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).forEach { it.boost(2) }
    }
}

object Unicorn : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.TOKEN, Tag.Beast)
    override val description = "Deals 2 damage to all unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - playerId).forEach { it.dealDamage(2) }
    }
}


private object YenneferConjurer : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Mage, Tag.Aedirn, Tag.Gold)
    override val description = "Deal 1 damage to the Highest enemies on turn end."

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - playerId).allMin { it.currentStrength }.forEach { it.dealDamage(1) }
    }
}


private object ZoltanScoundrel : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Dwarf, Tag.Gold, Tag.Beast)
    override val description = "Choose one: Spawn a Companion that boosts 2 adjacent units by 2; or Spawn an Agitator that deals 2 damage to 2 adjacent units. <"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(DudaAgitator, DudaCompanion))
    }


}

object DudaAgitator : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Dwarf, Tag.TOKEN, Tag.Beast)
    override val description: String = "Deal 2 damage to 2 adjacent units."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        listOf(location.left(), location.right()).forEach {
            state.getCardByLocation(it)?.dealDamage(2)
        }
    }
}

object DudaCompanion : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Dwarf, Tag.TOKEN, Tag.Beast)
    override val description: String = " Boost 2 adjacent units by 2. "
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        listOf(location.left(), location.right()).forEach {
            state.getCardByLocation(it)?.boost(2)
        }
    }
}

private object ArtefactCompression : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Spell, Tag.Silver, Tag.SPECIAL)
    override val description = "Transform a Bronze or Silver unit into a Jade Figurine.Jade Figurine: 2 Strength, Token"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny) { it.ist(Tag.Silver) || it.ist(Tag.Bronze) }?.transform(JadeFigurine)
    }


}

object JadeFigurine : AbstractCard() {
    override val defaultStrenght = 2
    override val tags = listOf(Tag.TOKEN)
    override val description = "Token"
}

private object BekkersDarkMirror : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = "Transfer up to 10 power between the Highest and Lowest unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val lowset = state.getAlCardsOnBattleFiled().allMin { it.currentStrength }.randomOrNull(state.getRng())
            ?: return
        val highest = state.getAlCardsOnBattleFiled().allMax { it.currentStrength }.randomOrNull(state.getRng())
            ?: return
        val transfer = (lowset.currentStrength - 1).coerceIn(0, 10)
        lowset.setCurrentStrenght(lowset.currentStrength - transfer)
        highest.boost(transfer)
    }
}


private object BekkersRockslide : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Silver)
    override val description = "Deal 2 damage to up to 10 random enemies"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId, mEnemy).pickUpToN(10, state.getRng()).forEach { it.dealDamage(2) }
    }
}


private object BlackBlood : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Silver, Tag.Alchemy)
    override val description = """Choose One:
 Create a Bronze (Necrophage or Vampire) and boost it by 2
 Destroy a (Bronze or Silver) (Necrophage or Vampire)"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0] == 0)
            state.createCard(playerId) { it.ist(Tag.Bronze) && (it.ist(Tag.Necrophage) || it.ist(Tag.Vampire)) }
        else
            state.pickCardOnBattleFiled(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && (it.ist(Tag.Necrophage) || it.ist(Tag.Vampire)) }?.destroy()
    }
}


private object CarloVarese : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Dwarf, Tag.Silver)
    override val description = "Deal 1 damage for each card in your hand"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(state.players[playerId].knownHand.size)
    }
}


private object CommandersHorn : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override val description = "Boost 5 adjacent units 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.getNAdjacent(2)?.forEach { it.boost(5) }
    }
}


private object CyprianWiley : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Redania, Tag.Silver)
    override val description = "Weaken a unit by 4"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.weaken(4)
    }
}


private object Decoy : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override val description = "Return a Bronze or Silver Ally to your Hand, Boost it by 3 and play it."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Silver) || it.ist(Tag.Bronze) }
            ?: return
        card.boost(3)
        card.addToHand(playerId)
        state.forceToPlay(playerId, filter = { it.uuid == card.uuid })
    }
}


private object DorregarayofVole : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Mage, Tag.Silver)
    override val description = "Create any Bronze or Silver Beast or Draconid"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && (it.ist(Tag.Beast) || it.ist(Tag.Draconid)) }

    }
}


private object DragonsDream : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Silver, Tag.Alchemy)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*Alchemy, Special, Item, Silver, Epic.
*Apply a Hazard to an enemy row that will explode and deal 4 damage to all units when a different special card is played. <u>Added to the game</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object AdrenalineRush : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = "Toggle a unit's Resilience status"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny) ?: return
        state.togleResilience(card)
    }
}


object AlzursThunder : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = "Deal 9 damage"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.dealDamage(9)
    }
}


object ArachasVenom : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze)
    override val description = "Damage 3 adjacent Units by 4"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.getNAdjacent(4)?.forEach {
            it.dealDamage(4)
        }
    }
}


object BitingFrost : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Bronze)
    override val description = "Apply a Hazard to an enemy row that deals 2 damage to the Lowest unit on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyWeather(state.pickRow(playerId, mEnemy), FrostWether())
    }
}


object ClearSkies : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = "Choose One: Boost all damaged allies under Hazards by 2 and clear all Hazards from your side; or Play a random Bronze unit from your deck"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, listOf("Boost all damaged allies under Hazards by 2 and clear all Hazards from your side", "Play a random Bronze unit from your deck"))[0]) {
            0 -> (0..2).forEach { row ->
                if (state.players[playerId].weathers[row] != null) {
                    state.applyWeather(CardLocation(playerId, row, 0), null)
                    state.players[playerId].rows[row].forEach { it.boost(2) }
                }
            }
            1 -> state.summonRandom(playerId) { it.ist(Tag.Bronze) }
        }
    }
}


private object CrowsEye : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze, Tag.Alchemy)
    override val description = "Deal 4 damage to the Highest enemy on each row. Deal 1 extra damage for each copy of this card in your graveyard. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val dmg = 3 + state.players[playerId].graveyard.count { it.name == thisCard.name }
        state.players[1 - playerId].rows.forEach { list ->
            val highest = list.maxByOrNull { it.baseStrength }
            if (highest != null)
                list.filter { it.baseStrength == highest.baseStrength }.forEach { it.dealDamage(dmg) }
        }
    }
}


private object DimeritiumBomb : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Silver, Tag.Alchemy)
    override val description = "Reset all boosted units on a row"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAny)
        state.getRowByLocation(row).filter { it.currentStrength > it.baseStrength }.forEach { it.reset() }
    }
}


private object DimeritiumShackles : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Bronze, Tag.Alchemy)
    override val description = "Lock a unit. If an enemy, deal 4 damage to it"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAny)
        if (card != null) {
            card.toggleLock()
            if (card.location().playerId != playerId)
                card.dealDamage(4)
        }
    }
}


private object Doppler : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = "Spawn a random Bronze unit from your faction"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(state.players[playerId].fraction) && it.ist(Tag.Bronze) }
    }
}


object Epidemic : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze)
    override val description = "Destroy all the Lowest units"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players.map { it.rows }.flatten().flatten().allMin(Card::currentStrength).forEach {
            it.destroy()
        }

    }
}


object ImpenetrableFog : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Bronze)
    override val description = "Apply a Hazard to an enemy row that deals 2 damage to the Highest unit on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mEnemy)
        state.applyWeather(row, FogWeather())
    }
}


private object Lacerate : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze)
    override val description = "Damage all Units on a row by 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAny)
        state.getRowByLocation(row).toMutableList().forEach {
            it.dealDamage(3)
        }
    }
}


private object MahakamAle : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Bronze)
    override val description = "Boost a random Ally on each row by 4"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].rows.filter { it.isNotEmpty() }.forEach {
            it[state.getRng().nextInt(0, it.size)].boost(4)
        }
    }
}


private object MastercraftedSpear : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Bronze)
    override val description = "Deal damage equal to the base power of a Bronze or Silver unit in your hand"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val power = state.inHandValue(playerId, { !it.ist(Tag.SPECIAL) }) { it.baseStrength } ?: return
        state.pickCardOnBattleFiled(playerId)?.dealDamage(power)
    }
}


object PeasantMilitia : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze, Tag.TOKEN)
    override val description = "Spawn 3 Peasants on a row.Peasant: 3 Strength, Token, Bronze. No ability. Added to the game"


    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mAlly)
        repeat(3) {
            Peasant.spawn(state, playerId, forceedLocation = row)
        }
    }
}


object Peasant : AbstractCard() {
    override val tags = listOf(Tag.TOKEN, Tag.Bronze)
    override val defaultStrenght = 3
    override val description = "Token"

}

private object PetrisPhilter : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Item, Tag.Bronze)
    override val description = "Boost up to 6 random allies by 2"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).pickUpToN(6, state.getRng()).forEach { it.boost(2) }
    }
}


private object RockBarrage : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Rock Barrage</u>, Special, Bronze, Rare.
*Deal 7 damage to an enemy and move it to the row above. If the row is full, destroy the enemy instead. <u>Removed Organic tag</u>.}}


*"""

    //TODO row full
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object StammelfordsTremors : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Spell, Tag.Bronze)
    override val description = "Deal 1 damage to all enemies"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - playerId).forEach { it.dealDamage(1) }
    }
}


private object Swallow : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Item, Tag.Bronze)
    override val description = "Boost a unit by 10"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.boost(10)
    }
}


object Thunderbolt : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Item, Tag.Bronze)
    override val description = "Boost 3 adjacent units by 3 and give them 2 Armor"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.getNAdjacent(1)?.forEach {
            it.boost(3)
            it.giveArmour(2)
        }


    }
}


object TorrentialRain : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Hazard, Tag.Bronze)
    override val description = "Apply a Hazard to an enemy row that deals 1 damage to 2 random units on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, mEnemy)
        state.applyWeather(row, RainWeather())
    }
}


private object Watchman : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Nilfgaard, Tag.Soldier, Tag.Bronze)
    override val description = "Boost all copies of a Soldier by 2"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        state.boostAllInDeckAndHand(playerId, 2) { it.name == card.name }
        state.getAlCardsOnBattleFiled(playerId).filter { it.name == card.name }.forEach { it.boost(2) }
    }
}

//
//object WolfPack : AbstractCard() {
//    override var defaultStrenght = 1
//    override val tags = listOf(Tag.Beast, Tag.TOKEN, Tag.Bronze)
//    override val description = "Token"
//
//    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
//    }
//}


private object WyvernScaleShield : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Item, Tag.Bronze)
    override val description = "Boost a unit by the base power of a Bronze or Silver unit in your hand"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val unit = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        val card = state.inHandValue(playerId, { it.ist(Tag.Bronze) || it.ist(Tag.Silver) }) { it.baseStrength }
            ?: return
        unit.boost(card)
    }
}


private object BloodcurdlingRoar : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Organic, Tag.Bronze)
    override val description = "Destroy an ally. Spawn a Bear. Bear: 11 Strength, Beast, Cursed, Bronze"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly)
        if (card != null) {
            Bear.spawn(state, playerId)
            card.destroy()
        }
    }
}

object Bear : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.TOKEN, Tag.Beast, Tag.Cursed)
    override val description = "Token"

}

private object Mardroeme : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Organic, Tag.Bronze)
    override val description = """Choose One:
Reset a unit and Strengthen it by 3
Reset a unit and Weaken it by 3
"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val pick = state.pickNofM(playerId, 1, description.split("\n").drop(1))[0]
        val card = state.pickCardOnBattleFiled(playerId)
        if (card != null)
            if (pick == 0) {
                card.reset()
                card.strengthen(3)
            } else {
                card.reset()
                card.weaken(3)
            }
    }
}


object Overdose : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Alchemy, Tag.Item, Tag.Bronze)
    override val description = "Deal 2 damage to up to 6 random enemies"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[1 - playerId].rows.flatten().pickUpToN(6, state.getRng()).forEach { it.dealDamage(2) }
    }
}


private object Warcry : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.SPECIAL, Tag.Bronze)
    override val description = "Double the strength of all weakened non-Gold units on your side"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].rows.flatten().filter { it.baseStrength < it.ac.defaultStrenght }.forEach { it.boost(it.currentStrength) }
    }
}


