package walic.gwentBeta


import walic.pureEngine.*

val monsterLeaders = listOf(
    ArachasQueen,
    Dagon,
    EredinBreaccGlas,
    UnseenElder
)
val mosterCards = listOf(
    WoodlandSpirit,
    WhisperingHillock,
    Imlerith,
    ImlerithSabbath,
    Katakan,
    Kayran,
    Miruna,
    OldSpeartip,
    OldSpeartipAsleep,
    WeavessIncantation,
    WhispessTribute,
    Abaya,
    Brewess,
    FrightenerDormant,
    ImperialManticore,
    MonsterNest,
    Ozzrel,
    Ruehin,
    Weavess,
    Whispess,
    AncientFoglet,
    ArachasDrone,
    Archgriffin,
    Barbegazi,
    BridgeTroll,
    Chort,
    Cockatrice,
    Cyclops,
    Drowner,
    Fiend,
    Forktail,
    Ghoul,
    IceGiant,
    Lamia,
    NekkerWarrior,
    PredatoryDive,
    Rotfiend,
    Siren,
    Slyzard,
    Werecat,
    WildHuntHound,
    WildHuntNavigator,
    WildHuntWarrior,
    Wyvern,
    Naglfar,
    Jotunn,
    Nithral,
    Parasite,
    Archespore,
    VranWarrior,
    IceTroll,
    Geels
)

object ArachasQueen : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Leader, Tag.Insectoid, Tag.IMMUNNE)
    override val description = "Consume 3 allies and boost self by their power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            state.consume(thisCard, state.pickCardOnBattleFiled(playerId, mAlly) ?: return)
        }
    }
}


object Dagon : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Leader, Tag.Vodyanoi)
    override val description = "Spawn Fog or Rain."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(ImpenetrableFog, TorrentialRain))
    }
}


object EredinBreaccGlas : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Leader, Tag.WildHunt)
    override val description = "Spawn a Bronze Wild Hunt unit."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId, 1) { it.ist(Tag.Bronze) && it.ist(Tag.WildHunt) }
    }
}


object UnseenElder : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Leader, Tag.Vampire)
    override val description = "Drain a unit by half"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drain(state.pickCardOnBattleFiled(playerId, mAny) ?: return, thisCard, 0.5)
    }
}


object WoodlandSpirit : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Gold)
    override val description = """*Spawn 3 Wolves and apply a Fog to the opposite row.
*<u>Wolf</u>: 1 Strength, Beast, <u>TOKEN</u>, Bronze. """

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyWeather(thisCard.location(), FogWeather())
        repeat(3) {
            Wolf.spawn(state, playerId, thisCard.location())
        }

    }
}

object Wolf : AbstractCard() {
    override val defaultStrenght = 1
    override val tags = listOf(Tag.Beast, Tag.TOKEN, Tag.Bronze)
    override val description = "Token"

}


object WhisperingHillock : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Leader, Tag.Relict)
    override val description = "Create a Bronze or Silver Organic card"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && it.ist(Tag.Organic) }
    }
}


object Draug : AbstractCard() {
    //TODO max per row
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Cursed, Tag.Officer, Tag.Gold)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>10 Strength</u>, <u>Cursed</u>, Officer, Gold, Legendary.
*<u>Resurrect units as 1-power Draugirs until you fill this unit's row</u>. <u>Removed Specter Tag</u>.
*Draugir: 1 Strength, <u>Cursed</u>, <u>TOKEN</u>, Bronze. <u>No ability</u>. <u>Removed Soldier and Specter Tags</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}

object Draugir : AbstractCard() {
    override val defaultStrenght = 1
    override val tags = listOf(Tag.Cursed, Tag.TOKEN, Tag.Bronze)
    override val description = "Token"

}


object Geels : AbstractCard() {
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Monsters, Tag.Officer, Tag.Gold)
    override val description = "Draw the top Gold card and top Silver card from your Deck. Play one and return the other to the top of your Deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val cards = listOf(Tag.Gold, Tag.Silver).mapNotNull { tag -> state.getTopFromDeck(playerId) { it.ist(tag) } }
        val card = state.pickOneOfCard(playerId,cards)
        card.playCardwithBattlecryAndEverything(playerId,PlayMod.FromDeck)
        cards.filter { it!=card }.forEach { it.addToDeck(playerId, DeckLocation.TOP) }


    }
}


object Imlerith : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Monsters, Tag.Officer, Tag.Gold)
    override val description = " Damage an Enemy by 4. If the Enemy is under a Frost <u>Hazard</u>, Damage it by 8 instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.apply {
            dealDamage(if (wetherUnder() is FrostWether) 8 else 4)
        }
    }
}


object ImlerithSabbath : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Officer, Tag.Gold)
    override val description = "Every turn, Duel the Highest enemy on turn end. If this unit survives, Heal it by 2 and give it 2 Armor. 2 Armor."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(2)
    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        val target = state.getAlCardsOnBattleFiled(1 - playerId).allMax { it.currentStrength }.random(state.getRng())
        state.duel(thisCard, target)
        thisCard.giveArmour(2)
        thisCard.heal(2)
    }
}


object Katakan : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Vampire, Tag.Silver)
    override val description = "Spawn Moonlight"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        Moonlight.spawn(state, playerId)
    }
}


object Kayran : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Insectoid, Tag.Gold)
    override val description = "Consume a unit with <u>7 power</u> or less and boost self by its power"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.consume(thisCard, state.pickCardOnBattleFiled(playerId, mAny) { it.currentStrength <= 7 } ?: return)
    }
}


object Miruna : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Gold)
    override val description = "After 2 turns, Charm the Highest enemy on the opposite row on turn start"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.counter = 2
    }

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter -= 1
        if (thisCard.counter == 0) {
            state.getAlCardsOnBattleFiled(1 - playerId).allMax { it.currentStrength }.random(state.getRng()).charm()
        }
    }
}


object Naglfar : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Machine, Tag.Bronze, Tag.WildHunt)
    override val description = " Boost all Wild Hunt allies by 2."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).filter { it.ist(Tag.WildHunt) }.forEach { it.boost(2) }
    }
}


object OldSpeartip : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Gold)
    override val description = "Deal 2 damage to up to 5 enemies on the opposite row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getRowByLocation(thisCard.location()).pickUpToN(5, state.getRng()).forEach {
            it.dealDamage(2)
        }
    }
}


object OldSpeartipAsleep : AbstractCard() {
    override var defaultStrenght = 12
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Gold)
    override val description = "Strengthen all your other Ogroids in hand, deck, and on board by 1."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostAllWhereeverTheyAre(playerId, { it.ist(Tag.Ogroid) }, 1)
    }
}


object RitualSacrifice : AbstractCard() {
    //TODO Deathwish units
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Mage, Tag.Doomed, Tag.Gold)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*Brewess: Ritual, 1 Strength, Relict, Mage, Doomed, Gold, Legendary.
*Resurrect 2 Bronze Deathwish units. <u>Added to the game</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
//            state.pickFromGraviard(playerId){}
        }
    }
}


object WeavessIncantation : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Mage, Tag.Gold)
    override val description = """Choose One: 
Strengthen allied Relicts by 2, wherever they are //Right now boosts
Play a Bronze or Silver Relict from your deck and Strengthen it by 2."""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0] == 0) {
            state.boostAllWhereeverTheyAre(playerId, { it.ist(Tag.Relict) }, 2)
        } else {
            state.summon(playerId) { it.ist(Tag.Relict) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
        }
    }
}


object WhispessTribute : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Mage, Tag.Gold)
    override val description = "Play a Bronze or Silver Organic card from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver) && it.ist(Tag.Organic)) }
    }
}


object Abaya : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Silver)
    override val description = "Spawn Rain, Clear Skies or Arachas Venom"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(TorrentialRain, ClearSkies, ArachasVenom))
    }
}


object Brewess : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Mage, Tag.Relict, Tag.Silver)
    override val description = "Summon Whispess and Weavess"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Whispess)
        state.summonRandom(playerId, Weavess)
    }
}


object FrightenerDormant : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Monsters, Tag.Construct, Tag.Agent, Tag.Silver, Tag.SPY, Tag.SINGLEUSE)
    override val description = "Spying. Single-Use. Move an enemy to this unit's row and draw a card. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mEnemy)?.move(thisCard.location())
        state.draw(playerId, 1)
    }
}


object ImperialManticore : AbstractCard() {
    override var defaultStrenght = 13
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Silver)
    override val description = "No Ability"

}


object Jotunn : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Silver)
    override val description = " Move 3 Enemies to this row on their side and Damage them by 2. If that row is affected by Frost <u>Hazard</u>, Damage them by 3 instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            state.pickCardOnBattleFiled(playerId, mEnemy - maskOnlyRow(playerId, thisCard.location()))?.apply {
                move(thisCard.location().opositeRow())
                dealDamage(if (wetherUnder() is FrostWether) 3 else 2)
            }
        }

    }
}


object Maerolorn : AbstractCard() {
    //TODO Deathwish units
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Silver)
    override val description = """Patch|Feb 9, 2018|0.9.20 Mahakam Season.
*<u>Maerolorn</u>, 4 Strength, <u>Relict</u>, Silver, Epic.
*Play a Bronze Deathwish unit from your Deck. <u>Removed Necrophage Tag</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object MonsterNest : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Monsters, Tag.SPECIAL, Tag.Organic, Tag.Silver)
    override val description = "Spawn a Bronze Necrophage or Insectoid unit and boost it <u>by 1</u>.}}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId, 1) { it.ist(Tag.Necrophage) || it.ist(Tag.Insectoid) }?.boost(1)
    }
}


object Morvudd : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Silver)
    override val description = "Toggle a unit's Lock status. If it was an enemy, halve its power."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.apply {
            toggleLock()
            if (owner != playerId) {
                val cs = currentStrength
                setBaseStreanght(baseStrength / 2)
                setCurrentStrenght(cs / 2)
            }
        }
    }
}


object Mourntart : AbstractCard() {
    //TODO Consume in graveyard
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Silver)
    override val description = "Consume all Bronze and Silver units in your graveyard and boost self by 1 for each."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].graveyard.filter { !it.ist(Tag.SPECIAL)&&(it.ist(Tag.Silver)||it.ist(Tag.Bronze) ) }.forEach {
            state.consume(thisCard,it,base = true)
        }
    }
}


object Nithral : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Officer, Tag.Silver, Tag.WildHunt)
    override val description = "*Deal <u>6 damage</u> to an enemy. <u>Increase damage by 1 for each Wild Hunt unit in your hand</u>"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(6 + state.getInfoAboutHand(playerId) { list, _ -> list.count { it.ist(Tag.WildHunt) } })
    }
}


object Ozzrel : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Silver)
    override val description = "Consume a Bronze or Silver unit from either graveyard and boost by its power."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Any)?.apply {
            state.consume(thisCard, this)
        }
    }
}


object Parasite : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Monsters, Tag.SPECIAL, Tag.Organic, Tag.Silver)
    override val description = """Choose One: 
Deal 12 damage to an enemy
Boost an ally by 12"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        when (state.pickNofM(playerId, 1, description.split("\n").drop(1))[0]) {
            0 -> state.pickCardOnBattleFiled(playerId)?.dealDamage(12)
            1 -> state.pickCardOnBattleFiled(playerId, mAlly)?.boost(12)
        }
    }
}


object Ruehin : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Insectoid, Tag.Cursed, Tag.Silver)
    override val description = "Strengthen all your Insectoid and Cursed units by 1, wherever they are."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostAllWhereeverTheyAre(playerId, { it.ist(Tag.Insectoid) || it.ist(Tag.Cursed) }, 1)
    }
}


object ToadPrince : AbstractCard() {
    //TODO Consume  in hand
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Cursed, Tag.Silver)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>6 Strength</u>, <u>Agile</u>, Cursed, Silver, Epic.
*Draw a <u>unit</u>, then Consume a unit in your hand and boost self by its power.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object Weavess : AbstractCard() {
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Monsters, Tag.SPECIAL, Tag.Mage, Tag.Relict, Tag.Silver)
    override val description = "Summon Brewess and Whispess."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Brewess)
        state.summonRandom(playerId, Whispess)
    }
}


object Whispess : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Mage, Tag.Relict, Tag.Silver)
    override val description = "Summon Brewess and Weavess."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Brewess)
        state.summonRandom(playerId, Weavess)
    }
}


object AlphaWerewolf : AbstractCard() {
    //TODO  on contact with wethar
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Cursed, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*10 Strength, Beast, Cursed, Bronze, Rare.
*Spawn a Wolf on either side on contact with Full Moon.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object AncientFoglet : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Bronze)
    override val description = "Boost by 1 if Fog is on the board on turn end."

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        if (state.players.flatMap { it.weathers }.any { it is FogWeather })
            thisCard.boost(1)
    }
}


object ArachasBehemoth : AbstractCard() {
    //TODO Whenever you Consume a unit
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Insectoid, Tag.Bronze, Tag.Insectoid, Tag.TOKEN)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>8 Strength</u>, Insectoid, Bronze, Rare.
*Whenever you Consume a unit, Spawn an Arachas Hatchling. <u>Repeat up to 3 times</u>. <u>Removed Armor</u>. <u>No longer damages self</u>.
*Arachas Hatchling: 3 Strength, Insectoid, <u>TOKEN</u>, Bronze. Summon all Arachas Drones. <u>Removed Doomed from ability section</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object ArachasDrone : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Monsters, Tag.Insectoid, Tag.Bronze)
    override val description = "Summon all copies of this unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonAllCopies(playerId, this, thisCard.location())
    }
}


object Archespore : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Cursed, Tag.Bronze)
    override val description = "Move to a random row and deal 1 damage to a random enemy on turn start. Deathwish: Deal 4 damage to a random enemy."

    override suspend fun onDeath(state: State, thisCard: Card) {
        state.getRandomCardOnBattlefiled(thisCard.owner)?.dealDamage(4)
    }

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        thisCard.move(state.getRng().nextInt(3))
        state.getRandomCardOnBattlefiled(playerId)?.dealDamage(1)
    }
}


object Archgriffin : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Bronze)
    override val description = "Clear Hazards on its row. "

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.applyWeather(thisCard.location(), null)
    }
}


object Barbegazi : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Insectoid, Tag.Bronze)
    override val description = "Consume an ally and boost self by its power. Resilient."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.resilience = true
        state.consume(thisCard, state.pickCardOnBattleFiled(playerId, mAlly) ?: return)

    }
}


object BridgeTroll : AbstractCard() {
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = "Move a Hazard on an enemy row to a different enemy row."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val row = state.pickRow(playerId, RowMask(listOf(false, false, false), state.players[1 - playerId].weathers.map { it != null }))
        val target = state.pickRow(playerId, mEnemy)
        val wether = state.players[1 - playerId].weathers[row.rowId]
        state.applyWeather(row, null)
        state.applyWeather(target, wether)
    }
}


object CelaenoHarpy : AbstractCard() {
    //TODO Harpy Eggs.
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Bronze, Tag.TOKEN, Tag.Beast, Tag.TOKEN)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>6 Strength</u>, Beast, Bronze, Common.
*Spawn 2 Harpy Eggs.
*Harpy Egg: 1 Strength, <u>TOKEN</u>, Bronze. When Consumed, boost by 4. Deathwish: Spawn a Harpy Hatchling on a random row. <u>Removed Doomed from ability section</u>.
*Harpy Hatchling: 1 Strength, Beast, <u>TOKEN</u>, Bronze. No ability. <u>Removed Doomed from ability section</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object Chort : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Bronze)
    override val description = "No Ability"

}


object Cockatrice : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Draconid, Tag.Bronze)
    override val description = "Reset a unit"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.reset()
    }
}


object Cyclops : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = "Destroy an ally and deal its power as damage."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) ?: return
        state.pickCardOnBattleFiled(playerId)?.dealDamage(card.currentStrength)
        card.destroy()
    }
}


object Drowner : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Bronze)
    override val description = "Move an <u>enemy</u> to the opposite row and deal 2 damage to it. If that row is under a Hazard, deal 4 damage instead."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.apply {
            move(thisCard.location().opositeRow())
            dealDamage(if (wetherUnder() != null) 4 else 2)

        }
    }
}


object Ekimmara : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Vampire, Tag.Bronze)
    override val description = "Drain a unit by 3"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
         state.drain(state.pickCardOnBattleFiled(playerId)?:return,thisCard,atMost = 3)
    }
}


object Fiend : AbstractCard() {
    override var defaultStrenght = 11
    override val tags = listOf(Tag.Monsters, Tag.Relict, Tag.Bronze)
    override val description = "No ability<"
}


object Foglet : AbstractCard() {
    //TODO summon fog
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*4 Strength, Necrophage, Bronze, Common.}}
*<fontsize "2">Whenever you apply Fog to an enemy row, Summon a copy of this unit from your deck. <u>Won't be destroyed when Fog is removed</u>. <u>Won't Resurrect self anymore</u>.</font>


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object Forktail : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Draconid, Tag.Bronze)
    override val description = "Consume 2 allies"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            state.consume(thisCard, state.pickCardOnBattleFiled(playerId) ?: return)
        }
    }
}


object Ghoul : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Bronze)
    override val description = "Consume a Bronze <u>or Silver</u> Unit from your Graveyard."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.consume(thisCard, state.pickFromGraviard(playerId) { !it.ist(Tag.SPECIAL) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
            ?: return)
    }
}


object Golem : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Monsters, Tag.Bronze, Tag.Fleeting)
    override val description = "On death from the battlefield, spawn 2 Lesser Golem units."

    override suspend fun onDeath(state: State, thisCard: Card) {
        repeat(3){
            LesserGolem.spawn(state,thisCard.owner,thisCard.location())
        }
    }
}

object LesserGolem : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Monsters, Tag.Bronze, Tag.Fleeting)
    override val description = "Tokne"


}

object Harpy : AbstractCard() {
    //TODO Summon destory
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>4 Strength</u>, Beast, Bronze, Rare.
*Summon a copy of this unit whenever you destroy an allied Beast. <u>Won't deal damage anymore</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object HarpyEgg : AbstractCard() {
    //TODO Consumed boost
    override var defaultStrenght = 1
    override val tags = listOf(Tag.Monsters, Tag.TOKEN, Tag.Beast, Tag.TOKEN)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*1 Strength, <u>TOKEN</u>, Bronze. 
*When Consumed, boost by 4. Deathwish: Spawn a Harpy Hatchling on a random row. <u>Removed Doomed from ability section</u>.
*Harpy Hatchling: 1 Strength, Beast, <u>TOKEN</u>, Bronze. No ability. <u>Removed Doomed from ability section</u>.}}


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object IceGiant : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = "If a Frost <u>Hazard</u> is anywhere on the Board, Boost self by 6."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.players[1 - playerId].weathers.any { it is FrostWether })
            thisCard.boost(6)
    }
}


object IceTroll : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = "Duel an enemy. If it is under Frost, deal double damage."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val target = state.pickCardOnBattleFiled(playerId) ?: return
        state.duel(thisCard, target, if (target.wetherUnder() is FrostWether) 2 else 1)
    }
}


object Lamia : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Bronze)
    override val description = "Deal 4 damage to an enemy. If the enemy is under Blood Moon, deal 7 damage instead"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.apply { dealDamage(if (wetherUnder() is BloodMoonWether) 7 else 4) }
    }
}


object Moonlight : AbstractCard() {
    //TODO zrobić
    override var defaultStrenght = -1
    override val tags = listOf(Tag.Monsters, Tag.SPECIAL, Tag.Hazard, Tag.Bronze)
    override val description = """Patch|Apr 17, 2018|0.9.23 Spring Update.
*Special, Hazard, Boon, Bronze, Rare.
*Choose One: Apply a Full Moon Boon; or Apply a Blood Moon Hazard.
*Full Moon: Special, Boon, Bronze. Apply a Boon to an allied row that boosts a random Beast or Vampire by 2 on turn start.
*Blood Moon: Special, Hazard, Bronze. Apply a Hazard to an enemy row that deals 2 damage to all units on contact. <u>Improved VFX</u>. <u>(The entire effect was darkened)</u>.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }
}


object Nekker : AbstractCard() {
    //TODO fuck this guy
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>4 Strength</u>, <u>Agile</u>, Ogroid, Bronze, Rare.}}
*<fontsize "2">Whenever you Consume a card, boost this unit by 1, wherever it is. Deathwish: Play a copy of this unit from your deck.</font>
*<fontsize "2"><u>Note: General change; Deathwish abilities no longer trigger at the end of the round</u>.</font>


*"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object NekkerWarrior : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Monsters, Tag.Ogroid, Tag.Bronze)
    override val description = "Choose a Bronze ally and add 2 copies of it to your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.apply {
            repeat(2) {
                defaultCopy(state.getNextUUID()).addToDeck(playerId)
            }
        }
    }
}


object PredatoryDive : AbstractCard() {
    override var defaultStrenght = 9
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Bronze)
    override val description = " Trigger the Deathwish of a Bronze Ally."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Bronze) }?.onDeath()
    }
}


object Rotfiend : AbstractCard() {
    override var defaultStrenght = 8
    override val tags = listOf(Tag.Monsters, Tag.Necrophage, Tag.Bronze)
    override val description = "Deathwish: Damage all units on the opposite row by 2."

    override suspend fun onDeath(state: State, thisCard: Card) {
        state.getRowByLocation(thisCard.location().opositeRow()).toMutableList().forEach {
            it.dealDamage(2)
        }
    }
}


object Siren : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Siren, Tag.Bronze)
    override val description = "Play Moonlight from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Moonlight)
    }
}


object Slyzard : AbstractCard() {
    override var defaultStrenght = 2
    override val tags = listOf(Tag.Monsters, Tag.Draconid, Tag.Bronze)
    override val description = "Consume a different Bronze unit from your graveyard, then play a copy of it from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickFromGraviard(playerId) { it.ist(Tag.Bronze) }?.apply {
            state.consume(thisCard, this)
            state.summonRandom(playerId, ac)
        }
    }
}


object VranWarrior : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Draconid, Tag.Soldier, Tag.Bronze)
    override val description = "Deploy: Consume the Unit to the right. Every 2 turns, at the start of your turn, Consume the Unit to the right.}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.consume(thisCard, state.getCardByLocation(thisCard.location().right()) ?: return)
        thisCard.counter = 2
    }

    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        thisCard.counter--
        if (thisCard.counter == 0) {
            thisCard.counter = 2
            state.consume(thisCard, state.getCardByLocation(thisCard.location().right()) ?: return)
        }

    }
}


object Werecat : AbstractCard() {
    override var defaultStrenght = 5
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Cursed, Tag.Bronze)
    override val description = "Deal 5 damage to an enemy, then deal 1 damage to all enemies under Blood Moon."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(1 - playerId).filter { it.wetherUnder() is BloodMoonWether }.forEach { it.dealDamage(1) }
    }
}


object Werewolf : AbstractCard() {
    //TODO  on contact with Full wether
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Beast, Tag.Cursed, Tag.Bronze)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*7 Strength, Beast, Cursed, Bronze, Rare.
*Boost by 7 on contact with Full Moon. Immune.
*Added to the game.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object WildHuntHound : AbstractCard() {
    override var defaultStrenght = 4
    override val tags = listOf(Tag.Monsters, Tag.Construct, Tag.Bronze, Tag.WildHunt)
    override val description = "Deploy: Play a Biting Frost card from your Deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, BitingFrost)
    }
}


object WildHuntNavigator : AbstractCard() {
    override var defaultStrenght = 3
    override val tags = listOf(Tag.Monsters, Tag.Mage, Tag.Bronze, Tag.WildHunt)
    override val description = "Choose a non-Mage Wild Hunt ally and play a copy of it from your deck."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(
            playerId, state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.WildHunt) && !it.ist(Tag.Mage) }?.ac
                ?: return
        )
    }
}


object WildHuntRider : AbstractCard() {
    //TODO damage dealt by Frost
    override var defaultStrenght = 10
    override val tags = listOf(Tag.Monsters, Tag.Soldier, Tag.Bronze, Tag.WildHunt)
    override val description = """Patch|Dec 19, 2017|0.9.18 Midwinter Major Update.
*<u>10 Strength</u>, Wild Hunt, Soldier, Bronze, Common.
*Increase the damage dealt by Frost on the opposite row by 1.}}


"""

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


object WildHuntWarrior : AbstractCard() {
    override var defaultStrenght = 7
    override val tags = listOf(Tag.Monsters, Tag.Soldier, Tag.Bronze, Tag.WildHunt)
    override val description = "Deal 3 damage to an enemy. If the enemy is destroyed or is under Frost, boost self by 2."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val enemy = state.pickCardOnBattleFiled(playerId) ?: return
        if (enemy.dealDamage(3) || thisCard.wetherUnder() is FogWeather)
            thisCard.boost(2)
    }
}


object Wyvern : AbstractCard() {
    override var defaultStrenght = 6
    override val tags = listOf(Tag.Monsters, Tag.Draconid, Tag.Bronze)
    override val description = "Deal 5 damage"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(5)
    }
}

            