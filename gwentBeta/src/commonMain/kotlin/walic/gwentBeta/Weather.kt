package walic.gwentBeta


import walic.pureEngine.*
import kotlin.random.Random


class RaghNarRoogWeather : Weather() {
    //Apply a Hazard to each enemy row that deals 2 damage to the Highest unit on turn start.
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.allMax { it.currentStrength }.randomOrNull(random)?.dealDamage(2)
    }
}

class FogWeather : Weather() {
    //Apply a Hazard to each enemy row that deals 2 damage to the Highest unit on turn start.
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.allMax { it.currentStrength }.randomOrNull(random)?.dealDamage(2)
    }
}

class RainWeather : Weather() {
    //Apply a Hazard to an enemy row that deals 1 damage to 2 random units on turn start
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.pickUpToN(2, random).forEach {
            it.dealDamage(1)
        }
    }
}

class KorathiHeatwaveWeather : Weather() {
    //"Apply a Hazard to each enemy row that deals 2 damage to the Lowest unit on turn start."
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.allMin { it.currentStrength }.randomOrNull(random)?.dealDamage(2)
    }
}

class FrostWether : Weather() {
    //Apply a Hazard to an enemy row that deals 2 damage to the Lowest unit on turn start
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.allMin { it.currentStrength }.randomOrNull(random)?.dealDamage(2)
    }
}

class GoldenFroth : Weather() {
    //Booostall units by 2
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.randomOrNull(random)?.boost(2)
    }
}

class SkelligeStormWether : Weather() {
    //Apply a Hazard to an enemy row that deals 2, 1 and 1 damage to the leftmost units on the row on turn start.

    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.getOrNull(0)?.dealDamage(2)
        row.getOrNull(1)?.dealDamage(1)
        row.getOrNull(2)?.dealDamage(1)
    }
}
class BloodMoonWether : Weather(){
    override suspend fun onTurn(state: State, row: List<Card>, random: Random) {
        row.getOrNull(0)?.dealDamage(2)
        row.getOrNull(1)?.dealDamage(1)
        row.getOrNull(2)?.dealDamage(1)
    }
}