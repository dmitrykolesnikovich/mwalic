package walic.gwentBeta

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

import kotlin.random.Random
import com.soywiz.korio.net.*
import com.soywiz.korio.stream.*
import walic.pureEngine.*

val allTockens = listOf(
    LesserIfrit,
    Bear,
    Chironex,
    Cow,
    DudaAgitator,
    DudaCompanion,
    JadeFigurine,
    LesserDao,
    Peasant,
    PeasantMilitia,
    ShupeKnight,
    ShupeMage,
    SteaShupeHunter,
    Unicorn,
    CowCarcass,
    LesserGuardian,
    Hemdall,
    LordOfUndvik,
    SpectralWhale,
    Specter,
    LeftFuckingInfantry,
    RightFuckingInfantry,
    Wolf,
    Draugir,
)
val spawners = listOf(
    RoachSprawner,
    BlueStripeCommandoSpawner,
    DunBannerSprawner,
    BloodyBaronSprawner,
    ImperialGolemSpawner,
)


fun initGwentBeta() {
    allNormalCards = allNKCards + allNeutralCards + nilfCards + skeligeCard
    allLeaders = nkLeaders + nilfLeaders + skeligeLeders
    allPossibleCards = allNormalCards + allLeaders + allTockens
    allSpawner = spawners
}



const val defPort = 2137
suspend fun startGwentBeta(playerAdapter: PlayerAdapter, coroutineScope: CoroutineScope, hostname: String) {
    initGwentBeta()



    val (inS: suspend (String) -> Unit, outS: suspend () -> String, playerId) = when (hostname) {
        "auto" -> {
            val streams = listOf(Channel<String>(10), Channel(10))
            val s2 = State(SteamAdapter(streams[0]::receive, streams[1]::send, 1, RadnomAdapter(true, Random), 0))
            coroutineScope.launch {
                s2.mainLoop()
            }
            Triple<suspend (String) -> Unit, suspend () -> String, Int>(streams[0]::send, streams[1]::receive, 0)
        }
        "serwer" -> {
            println("Starting as server at port:$defPort")
            val socket = AsyncServer(defPort, "0.0.0.0").accept()

            Triple<suspend (String) -> Unit, suspend () -> String, Int>(
                { socket.writeStringz(it) },
                { socket.readStringz() },
                0
            )
        }
        else -> {
            println("Starting with host:$hostname and port:$defPort")
            val socket = AsyncClient(hostname, defPort)
            Triple<suspend (String) -> Unit, suspend () -> String, Int>(
                { socket.writeStringz(it) },
                { socket.readStringz() },
                1
            )
        }
    }
    State(SteamAdapter(outS, inS, playerId, playerAdapter, 0)).mainLoop()
}