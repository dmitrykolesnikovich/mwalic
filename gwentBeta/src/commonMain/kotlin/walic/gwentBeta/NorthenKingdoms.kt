package walic.gwentBeta

import walic.pureEngine.*


val nkLeaders = listOf(
    KingFoltest,
    KingHenselt,
    KingRadovidV,
    PrincessAdda
)
val allNKCards = listOf(
    Dandelion,
    JohnNatalis,
    PhilippaEilhart,
    Priscilla,
    SeltkirkofGulet,
    SigismundDijkstra,
    MargaritaofAretuza,
    PrinceStennis,
    PrincessPavetta,
    Thaler,
    Trollololo,
    VincentMeis,
    AedirnianMauler,
    Ballista,
    BatteringRam,
    BloodyFlail,
    DamnedSorceress,
    FieldMedic,
    KaedweniCavalry,
    PoorFuckingInfantry,
    ReinforcedBallista,
    SiegeTower,
    TemerianDrummer,
    TemerianInfantry,
    TridamInfantry,
    Trebuchet,
    Winch,
    WitchHunter,
    HubertRejk,
    KeiraMetz,
    Vandergrift,
    RonvidtheIncessant,
    Shani,
    VernonRoche,
    Dethmold,
    Nenneke,
    Odrin,
    Reinforcements,
    SabrinaGlevissig,
    SiledeTansarville,
    ZoriaRunestone,
    AretuzaAdept,
    BanArdTutor,
    BlueStripeScout,
    KaedweniRevenant,
    KaedweniSergeant,
    ReaverScout,
    RedanianElite,
    RedanianKnight,
    ReinforcedTrebuchet,
    SiegeMaster,
    SiegeSupport,
    Ves,
    KaedweniKnight,
    BlueStripeCommando,
    CursedKnight,
    Botchling,
    Lubberkin,
    Kiyan,
    SabrinasSpecter,
    RedanianKnightElect,
    TormentedMage,
    FoltestsPride,
    ReaverHunter,
    BloodyBaron,
    DunBanner,
)

private object KingFoltest : AbstractCard() {
    override val description = "Boost all other allies and your non-Spying units in hand and deck by 1."
    override val defaultStrenght = 5
    override val tags = listOf(Tag.NorthenKingdom, Tag.Leader, Tag.Temeria, Tag.Crewd)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.getAlCardsOnBattleFiled(playerId).filter { it != thisCard }.forEach { it.boost(1) }
        state.boostAllInDeckAndHand(playerId, 1) { !it.ist(Tag.SPY) && !it.ist(Tag.SPECIAL) }
    }
}

private object KingHenselt : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Choose a Bronze Machine or Kaedweni ally and play all copies of it from your deck"
    override val tags = listOf(Tag.NorthenKingdom, Tag.NorthenKingdom, Tag.Leader, Tag.Kaedwen, Tag.Crewd)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) {
            (it.ist(Tag.Bronze) && it.ist(Tag.Machine)) || it.ist(Tag.Kaedwen)
        }
        if (card != null)
            state.summonAllCopies(playerId, card.ac, null)
    }
}


private object KingRadovidV : AbstractCard() {
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Leader, Tag.Redania, Tag.Crewd)
    override val description = "Toggle 2 units' Lock statuses. If enemies, deal 4 damage to them. Crew."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            val c = state.pickCardOnBattleFiled(playerId, mAlly)
            if (c != null) {
                c.toggleLock()
                if (c.location().playerId != playerId)
                    c.dealDamage(4)

            }
        }
    }
}


private object PrincessAdda : AbstractCard() {
    override val defaultStrenght = 6
    override val description: String = "Create a Bronze or Silver Cursed unit"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Leader, Tag.Cursed)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.Cursed) && it.ist(Tag.NorthenKingdom) }
    }
}


private object BloodyBaron : AbstractCard() {
    override val defaultStrenght = 9
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Officer, Tag.Gold)
    override val description  = "If in hand, deck, or on board, boost self by 1 whenever an enemy is destroyed."

}
object BloodyBaronSprawner : AbstractCard() {
    override val defaultStrenght = 9
    override val tags = listOf(Tag.SPAWNER)
    override val description = ""
    override suspend fun onCardDamaged(state: State, card: Card, thisCard: Card) {
        state.boostAllWhereeverTheyAre(thisCard.owner,{it.ac== BloodyBaron },1)
    }

}


private object Dandelion : AbstractCard() {
    override val description = "Boost 3 units in your deck by 2."
    override val defaultStrenght = 11
    override val tags = listOf(Tag.NorthenKingdom, Tag.Support, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.actionOnDeck(playerId) { list, random ->
            list.pickUpToN(3, random).forEach { it.baseStrength += 2 }
        }
    }
}


private object JohnNatalis : AbstractCard() {
    override val description: String = "Play a Bronze or Silver Tactics card from your Deck"
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Officer, Tag.Temeria, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { it.ist(Tag.Tactic) && (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) }
    }
}


private object KeiraMetz : AbstractCard() {
    override val defaultStrenght = 6
    override val description: String = "Spawn Alzur's Thunder, Thunderbolt or Arachas Venom.}"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Mage, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(AlzursThunder, Thunderbolt, ArachasVenom))
    }
}


private object PhilippaEilhart : AbstractCard() {
    override val description = "Deal 5 damage to an enemy, then deal 4, 3, 2 and 1 damage to random enemies"
    override val defaultStrenght = 1
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Redania, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mEnemy)?.dealDamage(5)
        (4 downTo 1).forEach { state.getRandomCardOnBattlefiled(playerId, mEnemy)?.dealDamage(it) }
    }
}


private object Priscilla : AbstractCard() {
    override val description = "Boost 5 random allies by 3."
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Support, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

        state.getAlCardsOnBattleFiled(playerId).pickUpToN(5, state.getRng()).forEach { it.boost(3) }

    }
}


private object SeltkirkofGulet : AbstractCard() {
    override val description = "Duel an enemy. 3 Armor"
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Cursed, Tag.Aedirn, Tag.Officer, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(3)
        val card = state.pickCardOnBattleFiled(playerId, mEnemy)
        if (card != null)
            state.duel(thisCard, card)
    }
}


private object Shani : AbstractCard() {
    override val defaultStrenght = 4
    override val tags = listOf(Tag.NorthenKingdom, Tag.Redania, Tag.Doomed, Tag.Gold)
    override val description: String = "Resurrect a non-Cursed Bronze or Silver unit and give it 2 Armor"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickFromGraviard(playerId) { !it.ist(Tag.Cursed) && !it.ist(Tag.SPECIAL) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
        if (card != null) {
            card.resurrect(playerId)
            card.armour += 2
        }
    }
}


private object SigismundDijkstra : AbstractCard() {
    override val description = "Spying. Play 2 random cards from your deck"
    override val defaultStrenght = 4
    override val tags = listOf(Tag.NorthenKingdom, Tag.Redania, Tag.Gold, Tag.SPY)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(2) {
            state.summonRandom(playerId)
        }
    }
}


private object Vandergrift : AbstractCard() {
    override val description = "Deal 1 damage to all enemies. If a unit is destroyed, apply Ragh Nar Roog to its row"
    override val defaultStrenght = 7
    override val tags = listOf(Tag.NorthenKingdom, Tag.Kaedwen, Tag.Officer, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[1 - playerId].rows.forEachIndexed { lineId, line ->
            if (line.toMutableList().map { it.dealDamage(1) }.any { it }) {
                state.applyWeather(CardLocation(1 - playerId, lineId), RaghNarRoogWeather())
            }
        }
    }
}


private object VernonRoche : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Deal 7damage. <u>At the start of the game, add a Blue Stripes Commando to your deck<"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Officer, Tag.Gold)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mEnemy)?.dealDamage(7)
    }

    override fun onStartOfGame(sih: SecretInformationHandler) {
        sih.cardToDraw.add(Card(BlueStripeCommando, sih.uuid++, null))
    }
}


private object Botchling : AbstractCard() {
    override val defaultStrenght = 10
    override val tags = listOf(Tag.NorthenKingdom, Tag.Cursed, Tag.Silver)
    override val description: String = "Summon a Lubberkin"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Lubberkin)
    }
}


private object Dethmold : AbstractCard() {
    override val defaultStrenght = 4
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Kaedwen, Tag.Silver)
    override val description: String = "Spawn Rain, Clear Skies or Alzur's Thunder"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.spawnOneOF(playerId, listOf(TorrentialRain, ClearSkies, AlzursThunder))
    }
}


private object HubertRejk : AbstractCard() {
    override val description = "Drain all boosts from units in your deck."
    override val defaultStrenght = 7
    override val tags = listOf(Tag.NorthenKingdom, Tag.Vampire, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val drain = state.actionOnDeck(playerId) { list, _ ->
            var sum = 0
            list.forEach {
                if (it.currentStrength > it.baseStrength) {
                    sum += it.currentStrength - it.baseStrength
                    it.currentStrength = it.baseStrength
                }
            }
            return@actionOnDeck sum
        }
        thisCard.boost(drain)
    }
}


private object Lubberkin : AbstractCard() {
    override val defaultStrenght = 5
    override val tags = listOf(Tag.NorthenKingdom, Tag.Cursed, Tag.Silver)
    override val description: String = "Summon a Botchling"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId, Botchling)
    }
}


private object MargaritaofAretuza : AbstractCard() {
    override val description = "Reset a unit and toggle its Lock status"
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Mage, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val c = state.pickCardOnBattleFiled(playerId, mAny)
        if (c != null) {
            c.reset()
            c.toggleLock()
        }
    }
}


private object Nenneke : AbstractCard() {
    override val defaultStrenght = 10
    override val description = "Return 3 Bronze or Silver units from the graveyard to your deck."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Support, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(3) {
            state.pickFromGraviard(playerId) { (it.ist(Tag.Silver) || it.ist(Tag.Bronze)) && !it.ist(Tag.SPECIAL) }
                ?.addToDeck(playerId)
        }

    }
}


private object Odrin : AbstractCard() {
    override val defaultStrenght = 8
    override val description = "Move to a random row and boost all allies on it by 1 on turn start."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Kaedwen, Tag.Silver)
    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        val newLane = state.getRng().nextInt(0, 3)
        thisCard.move(newLane)
        state.players[playerId].rows[newLane].forEach { it.boost(1) }

    }

}


private object PrinceStennis : AbstractCard() {
    override val description = "Play a random non-Spying Bronze or Silver unit from your deck and give it 5 Armor. 3 Armor."
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Aedirn, Tag.Officer, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && !it.ist(Tag.SPY) }?.giveArmour(5)
        thisCard.giveArmour(3)
    }
}


private object PrincessPavetta : AbstractCard() {
    override val description = "Return each player's Lowest Bronze or Silver unit to their deck"
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Cintra, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        for (player in 0..1) {
            state.players[player].rows.flatten().filter { !it.tags.contains(Tag.Gold) }
                .minByOrNull { it.currentStrength }?.addToDeck(player)
        }
    }
}


private object Reinforcements : AbstractCard() {
    override val defaultStrenght = Int.MIN_VALUE
    override val description = "Play a Bronze or Silver Soldier, Machine, Officer or Support unit from your deck"
    override val tags = listOf(Tag.NorthenKingdom, Tag.SPECIAL, Tag.Tactic, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summon(playerId) { (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) && (it.ist(Tag.Soldier) || it.ist(Tag.Machine) || it.ist(Tag.Officer) || it.ist(Tag.Support)) }
    }
}


private object RonvidtheIncessant : AbstractCard() {
    override val defaultStrenght = 11
    override val description = "Resurrect on a random row with 1 power on turn end. Crew."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Kaedwen, Tag.Silver, Tag.Crewd)

    override suspend fun onTurnsEndInGraviard(state: State, playerId: Int, thisCard: Card) {
        thisCard.resurrect(playerId, random = true)
        thisCard.setBaseStreanght(1)
        thisCard.setCurrentStrenght(1)
    }

}


private object SabrinaGlevissig : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Deathwish: Set the power of all units on the row to the power of the Lowest unit on the row."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Kaedwen, Tag.Silver, Tag.SPY)
    override suspend fun onDeath(state: State, thisCard: Card) {
        val row = state.getRowByLocation(thisCard.location()).filter { it.currentStrength > 0 }
        val min = row.minByOrNull { it.currentStrength }?.currentStrength
        if (min != null)
            row.forEach { it.setCurrentStrenght(min) }
    }


}


private object SiledeTansarville : AbstractCard() {
    override val defaultStrenght = 4
    override val description = "Play special draw a card"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.forceToPlay(playerId, filter = { it.ist(Tag.SPECIAL) }))
            state.draw(playerId, 1)

    }
}


private object Thaler : AbstractCard() {
    override val description = "Spying, Draw 2 cards, keep one and return the other to your deck."
    override val defaultStrenght = 13
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Agent, Tag.Silver, Tag.SPY)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.drawNPickM(playerId, 2, 1, top = true, mod = DrawNPickMMods.ToHand)

    }
}


private object Trollololo : AbstractCard() {
    override val description = "Single-Use. Draw 2 cards, keep one and return the other to your deck"
    override val defaultStrenght = 11
    override val tags = listOf(Tag.NorthenKingdom, Tag.Ogroid, Tag.Redania, Tag.Silver, Tag.SINGLEUSE)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(9)
        state.drawNPick1(playerId, 2, true, DrawNPickMMods.ToHand)
    }
}


private object Ves : AbstractCard() {
    override val defaultStrenght = 12
    override val description = "Swap up to 2 cards"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Soldier, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.swap(playerId, ammount = 2)
    }
}


private object VincentMeis : AbstractCard() {
    override val description = "Destroy the Armor of all units, then boost self by half the value destroyed"
    override val defaultStrenght = 9
    override val tags = listOf(Tag.NorthenKingdom, Tag.Beast, Tag.Cursed, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.getAlCardsOnBattleFiled().sumBy { it.removeAllArmour() } / 2)

    }
}


private object ZoriaRunestone : AbstractCard() {
    override val description = "*Create a (Bronze or Silver) Northern Realms card"
    override val defaultStrenght = Int.MIN_VALUE
    override val tags = listOf(Tag.NorthenKingdom, Tag.Alchemy, Tag.SPECIAL, Tag.Item, Tag.Silver)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.createCard(playerId) { it.ist(Tag.NorthenKingdom) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
    }
}


private object AedirnianMauler : AbstractCard() {
    override val description = "Deal 4 damage to an enemy"
    override val defaultStrenght = 7
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Aedirn, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(4)
    }
}


private object AretuzaAdept : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Play a random Bronze Hazard card from your Deck"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Temeria, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.summonRandom(playerId) { it.ist(Tag.Hazard) && it.ist(Tag.Bronze) }
    }
}


private object Ballista : AbstractCard() {
    override val description =
        "Damage an Enemy and up to 4 other random Enemies with the same Power by 1. Fresh Crew: Repeat the Deploy ability"
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Machine, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(auxData.crewCount + 1) {
            val target = state.pickCardOnBattleFiled(playerId)
            if (target != null) {
                val targetPower = target.currentStrength
                state.players[1 - playerId].rows.flatten().filter { it.currentStrength == targetPower }
                    .pickUpToN(4, state.getRng()).forEach {
                        it.dealDamage(1)
                    }
            }
        }
    }
}


private object BanArdTutor : AbstractCard() {
    override val defaultStrenght = 9
    override val description = "Swap a card in your hand with a Bronze special card from your deck"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Kaedwen, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.swap(playerId, deckFilter = { it.ist(Tag.Bronze) && it.ist(Tag.SPECIAL) })
    }
}


private object BatteringRam : AbstractCard() {
    override val description =
        "Deal 3 damage. If the unit was destroyed, deal 3 damage to another unit. Crewed: Increase initial damage by 1."
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Machine, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickCardOnBattleFiled(playerId, mEnemy)?.dealDamage(auxData.crewCount + 3) == true) {
            state.pickCardOnBattleFiled(playerId, mEnemy)?.dealDamage(3)
        }
    }
}


private object BloodyFlail : AbstractCard() {
    override val description = "Deal 5 damage and Spawn a Specter."
    override val defaultStrenght = -1
    override val tags = listOf(Tag.NorthenKingdom, Tag.SPECIAL, Tag.Item, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.dealDamage(5)
        Specter.spawn(state, playerId)
    }


}

object Specter : AbstractCard() {
    override val description = "TOken"
    override val defaultStrenght = 5
    override val tags = listOf(Tag.NorthenKingdom, Tag.Cursed, Tag.TOKEN, Tag.Bronze)

}

private object BlueStripeCommando : AbstractCard() {
    override val defaultStrenght = 3
    override val description =        "Whenever a different Temerian ally with the same power is played, Summon a copy of this unit from your deck."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Temeria, Tag.Bronze)
}
object BlueStripeCommandoSpawner : AbstractCard() {
    override val defaultStrenght = 3
    override val description =      ""
    override val tags = listOf(Tag.SPAWNER)
    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (thisCard.ist(Tag.Temeria))
             state.summonRandom(card.owner){it.baseStrength==card.baseStrength && it.ac == BlueStripeCommando }
    }
}



private object BlueStripeScout : AbstractCard() {
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Temeria, Tag.Bronze, Tag.Crewd)
    override val description =        "Boost all Temerian allies and your non-Spying Temerian units in hand and deck with the same power as this unit by 1. Crew.}}"

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].rows.flatten().filter { it.ist(Tag.Temeria) }.forEach { it.boost(1) }
        val currentStrenght = thisCard.currentStrength
        state.boostAllInDeckAndHand(playerId, 1) { it.ist(Tag.Temeria) && it.currentStrength == currentStrenght }
    }
}


private object CursedKnight : AbstractCard() {
    override val defaultStrenght = 8
    override val description = "Transform a Cursed ally into a copy of this unit"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Aedirn, Tag.Bronze, Tag.Cursed)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.transform(CursedKnight)
    }
}


private object DamnedSorceress : AbstractCard() {
    override val description = "If there is a Cursed unit on this unit's row, deal 7 damage"
    override val defaultStrenght = 4
    override val tags = listOf(Tag.NorthenKingdom, Tag.Mage, Tag.Cursed, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.getRowByLocation(thisCard.location())
                .any { it.tags.contains(Tag.Cursed) && it != thisCard }
        )
            state.pickCardOnBattleFiled(playerId)?.dealDamage(7)
    }
}


private object DunBanner : AbstractCard() {
    override val defaultStrenght = 4
    override val description = "If you are losing by more than 25< on turn start, Summon this unit to a random row."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Kaedwen, Tag.Bronze)
}
object DunBannerSprawner : AbstractCard() {
    override val defaultStrenght = 4
    override val description = ""
    override val tags = listOf(Tag.SPAWNER)
    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        if (state.players[playerId].getStrenght()-state.players[1-playerId].getStrenght()<-25)
            state.summonRandom(playerId, DunBanner)
    }
}


private object FieldMedic : AbstractCard() {
    override val description = "Boost Soldier allies by 1."
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Support, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].rows.flatten().filter { it.tags.contains(Tag.Soldier) }.forEach {
            it.boost(1)
        }
    }
}


private object KaedweniCavalry : AbstractCard() {
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Kaedwen, Tag.Bronze)
    override val description = "Destroy a unit's Armor, then boost self by the amount destroyed."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.boost(state.pickCardOnBattleFiled(playerId, mAny) { it.armour != 0 }?.removeAllArmour() ?: return)

    }
}


private object KaedweniKnight : AbstractCard() {
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Kaedwen, Tag.Bronze)
    override val description = "Boost this unit by 5 if played from the deck. 2 Armor."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (auxData.playMod == PlayMod.FromDeck)
            thisCard.boost(5)
        else
            thisCard.giveArmour(2)
    }
}


private object KaedweniRevenant : AbstractCard() {
    override val defaultStrenght = 4
    override val description: String =
        "When you play your next Spell or Item, Spawn a Doomed default copy of this unit on its row once. 1 Armor"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Cursed, Tag.Kaedwen, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(1)
        thisCard.counter = 1
    }

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if ((card.ist(Tag.Spell) || card.ist(Tag.Item)) && card.owner == thisCard.owner && thisCard.counter > 0) {
            KaedweniRevenant.spawn(state, thisCard.owner, thisCard.location())
            thisCard.counter--
        }
    }
}


private object KaedweniSergeant : AbstractCard() {
    override val description = "Clear Hazards from its row. 3 Armor"
    override val defaultStrenght = 9
    override val tags = listOf(Tag.NorthenKingdom, Tag.Kaedwen, Tag.Officer, Tag.Bronze, Tag.Crewd)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        state.applyWeather(location, null)
        thisCard.giveArmour(3)
    }
}


private object PoorFuckingInfantry : AbstractCard() {
    override val description =
        "Spawn Left Flank Infantry and Right Flank Infantry to the left and right of this unit, respectively."
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Soldier, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        LeftFuckingInfantry.spawn(state, playerId, location)
        RightFuckingInfantry.spawn(state, playerId, location.right())

    }
}

object RightFuckingInfantry : AbstractCard() {
    override val description = ""
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Soldier, Tag.Bronze, Tag.TOKEN)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}

object LeftFuckingInfantry : AbstractCard() {
    override val description = ""
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Soldier, Tag.Bronze, Tag.TOKEN)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
    }
}


private object ReaverHunter : AbstractCard() {
    override val defaultStrenght = 6
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Redania, Tag.Bronze)
    override val description =
        "Boost all copies of this unit by 1, wherever they are. Repeat its ability whenever a copy of this unit is played."

    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.boostAllWhereeverTheyAre(playerId, { it.ac == this }, 1)
    }

    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.ac == thisCard.ac) {
            state.boostAllWhereeverTheyAre(thisCard.owner, { it.ac == this }, 1)
        }
    }
}


private object ReaverScout : AbstractCard() {
    override val defaultStrenght = 1
    override val description: String = "Choose a different Bronze ally and play a copy of it from your deck."
    override val tags = listOf(Tag.NorthenKingdom, Tag.Redania, Tag.Bronze, Tag.Support)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Bronze) && it != thisCard }?.apply {
            state.summonRandom(playerId, ac)
        }

    }
}


private object RedanianElite : AbstractCard() {
    override val defaultStrenght = 8
    override val description = "Whenever this unit's Armor reaches 0, boost self by 5. 4 Armor"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Redania, Tag.Soldier, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(4)
    }

    override suspend fun onArmomrLost(state: State, thisCard: Card) {
        thisCard.boost(5)
    }
}

private object RedanianKnight : AbstractCard() {
    override val defaultStrenght = 7
    override val description: String = "If that unit has no Armor, boost it by 2 and give it 2 Armor on turn <u>end</u>"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Redania, Tag.Soldier, Tag.Bronze)

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        if (thisCard.armour == 0) {
            thisCard.boost(2)
            thisCard.giveArmour(2)
        }
    }
}


private object ReinforcedBallista : AbstractCard() {
    override val description = "Damage an Enemy by 2. Fresh Crew: Repeat the Deploy ability"
    override val defaultStrenght = 7
    override val tags = listOf(Tag.NorthenKingdom, Tag.Machine, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val c = state.pickCardOnBattleFiled(playerId, mEnemy)
        repeat(auxData.crewCount + 1)
        {
            c?.dealDamage(2)
        }
    }
}


private object ReinforcedTrebuchet : AbstractCard() {
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Machine, Tag.Bronze)
    override val description = "Deal 1 damage to a random enemy on turn end}\""
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {

    }

    override suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {
        val target = state.getAlCardsOnBattleFiled(1 - playerId).randomOrNull(state.getRng())
        target?.dealDamage(1)
    }
}

private object SiegeMaster : AbstractCard() {
    override val defaultStrenght = 6
    override val description = "Heal an allied Bronze or Silver Machine and repeat its ability"
    override val tags = listOf(Tag.NorthenKingdom, Tag.Kaedwen, Tag.Support, Tag.Bronze, Tag.Crewd)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.pickCardOnBattleFiled(playerId, mAlly) { it.ist(Tag.Machine) }
        if (card != null) {
            card.heal()
            card.onEntry(AuxCallData(card.getNAdjacent(1).count { it.ist(Tag.Crewd) }, PlayMod.Called))
        }
    }
}


private object SiegeSupport : AbstractCard() {
    override val defaultStrenght = 7
    override val description = "Whenever an ally appears, boost it by 1. If it's a Machine, also give it 1 Armor."
    override suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {
        if (card.owner == thisCard.owner) {
            card.boost(1)
            if (card.ist(Tag.Machine))
                card.giveArmour(1)
        }
    }

    override val tags = listOf(Tag.NorthenKingdom, Tag.Support, Tag.Bronze, Tag.Crewd)

}


private object SiegeTower : AbstractCard() {
    override val description = "Boost self by 2. Repet for each crew"
    override val defaultStrenght = 8
    override val tags = listOf(Tag.NorthenKingdom, Tag.Bronze, Tag.Temeria)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(1 + auxData.crewCount) {
            thisCard.boost(2)
        }
    }
}


private object TemerianDrummer : AbstractCard() {
    override val description = "Boost an ally by 6"
    override val defaultStrenght = 5
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Support, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAlly)?.boost(6)
    }
}


private object TemerianInfantry : AbstractCard() {
    override val description = "Summon all copies of this unit"
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Temeria, Tag.Soldier, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val location = thisCard.location()
        state.summonAllCopies(playerId, this, location)
    }
}


private object TridamInfantry : AbstractCard() {
    override val description = "4 Armor."
    override val defaultStrenght = 10
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        thisCard.giveArmour(4)
    }
}


private object Trebuchet : AbstractCard() {
    override val description = "Damage 3 Adjacent Enemies by 1. Fresh Crew: Increase the Damage dealt by 1."
    override val defaultStrenght = 7
    override val tags = listOf(Tag.NorthenKingdom, Tag.Machine, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId)?.getNAdjacent(1)
            ?.forEach { it.dealDamage(1 + auxData.crewCount) }


    }
}

private object Winch : AbstractCard() {
    override val description = "Boost all machines on your side of the board by 3"
    override val defaultStrenght = Int.MIN_VALUE
    override val tags = listOf(Tag.NorthenKingdom, Tag.Tactic, Tag.SPECIAL, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.players[playerId].rows.flatten().filter { it.tags.contains(Tag.Machine) }.forEach {
            it.boost(3)
        }
    }
}


private object WitchHunter : AbstractCard() {
    override val description = "Reset a Unit. If it's a Mage, play another Witch Hunter from your Deck."
    override val defaultStrenght = 3
    override val tags = listOf(Tag.NorthenKingdom, Tag.Soldier, Tag.Redania, Tag.Bronze)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        state.pickCardOnBattleFiled(playerId, mAny)?.apply {
            reset()
            if (tags.contains(Tag.Mage))
                state.summonRandom(playerId, WitchHunter)
        }

    }
}


object Kiyan : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Create a Bronze or Silver Alchemy card or Play a Bronze or Silver Item from your deck."
    override val tags = listOf(Tag.Gold, Tag.NorthenKingdom, Tag.Cursed, Tag.Witcher)
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        if (state.pickIf(playerId, "Create a Bronze or Silver Alchemy card")) {
            state.createCard(playerId) { it.ist(Tag.Alchemy) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
        } else {
            state.summon(playerId) { it.ist(Tag.Item) && (it.ist(Tag.Bronze) || it.ist(Tag.Silver)) }
        }
    }
}


object SabrinasSpecter : AbstractCard() {
    override val defaultStrenght = 3
    override val description = "Resurrect a Bronze Cursed unit."
    override val tags = listOf(Tag.Silver, Tag.NorthenKingdom)
    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        state.pickFromGraviard(playerId, TargetMods.Ally) { it.ist(Tag.Bronze) && it.ist(Tag.Cursed) }
            ?.resurrect(playerId)
    }
}

object RedanianKnightElect : AbstractCard() {
    override val defaultStrenght = 5
    override val description = "If this Unit has Armor at the start of your turn, Boost Units adjacent to it by 1"
    override val tags = listOf(Tag.Bronze, Tag.NorthenKingdom)
    override suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {
        if (thisCard.armour > 0) {
            thisCard.getNAdjacent(1).filter { it != thisCard }.forEach {
                it.boost(1)
            }
        }
    }

}

object TormentedMage : AbstractCard() {
    override val defaultStrenght = 2
    override val tags = listOf(Tag.Mage, Tag.Cursed, Tag.Bronze, Tag.NorthenKingdom)
    override val description = "Look at 2 Bronze Spells or Items from your deck, then play one"
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        val card = state.drawNPick1(
            playerId,
            2,
            false,
            DrawNPickMMods.ToGame
        ) { it.ist(Tag.Bronze) && (it.ist(Tag.Item) || it.ist(Tag.Spell)) }
            ?: return
        card.playCardwithBattlecryAndEverything(playerId, PlayMod.FromDeck)
    }

}


object FoltestsPride : AbstractCard() {
    override val defaultStrenght = 10
    override val tags = listOf(Tag.Machine, Tag.Silver, Tag.NorthenKingdom)
    override val description = "Damage an enemy by 2 and move it to the row above. Crewed: Repeat its ability."
    override suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {
        repeat(auxData.crewCount) {
            val card = state.pickCardOnBattleFiled(playerId) ?: return
            if (card.dealDamage(2))
                return
            val cLane = card.location().rowId
            if (cLane != 2)
                card.move(cLane + 1)
        }
    }
}

