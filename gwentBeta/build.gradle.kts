plugins {
    kotlin("multiplatform")
}


version = "unspecified"

repositories {
    mavenCentral()
    maven(url ="https://dl.bintray.com/korlibs/korlibs/")
}

kotlin {
    /* Targets configuration omitted. 
    *  To find out how to configure the targets, please follow the link:
    *  https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#setting-up-targets */
    jvm()
    js {
        browser {}
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(project(":pureEngine"))
                implementation("com.soywiz.korlibs.korio:korio:2.0.3")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
            }
        }
    }
}