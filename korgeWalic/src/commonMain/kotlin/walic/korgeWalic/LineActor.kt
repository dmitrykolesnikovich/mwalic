package walic.korgeWalic


import com.soywiz.korge.input.onClick
import com.soywiz.korge.input.onOut
import com.soywiz.korge.input.onOver
import com.soywiz.korge.view.*
import com.soywiz.korim.bitmap.slice
import walic.pureEngine.Card
import walic.pureEngine.CardLocation
import walic.pureEngine.Tag
import kotlin.math.abs
import kotlin.math.roundToInt


const val midNone = 700.0


class LineActor(
    private val master: RubeGoldbergRenderer,
    val list: List<Card>,
    private val baseText: String,
    private val defY: Double
) : CardGroupAdapter() {

    var location = CardLocation(-1, -1, -1)
    var wetherText: String
        get() = wetherTx.text
        set(value) {
            wetherTx.text = value
            wetherTx.alignLeftToRightOf(im)
            wetherTx.centerYOn(im)
        }
    var laterGetter: GetLater<CardLocation>? = null

    private fun isCathing(): Boolean {
        return master.mouseFollower != null && laterGetter != null
    }

    init {
        master.stage.addChild(this)
        this.y = defY
        this.x = midNone
    }

    private val im = image(frontBitmap) {
        val h = ySep
        height = h
        y = -h / 2


        val w = xSep
        width = w
        x = -w / 2

    }
    private val strTx = master.stage.text("") {

    }
    private val wetherTx = master.stage.text("")


    fun refersh() {
        generateCards(list)
    }


    var delta = 0.0
    var scaledWidthMult = 1.0
    var lastSize = 1

    private fun generateCards(cardList: List<Card>) {
        scaledWidthMult = cardList.size + 0.5
        delta = midNone - (cardList.size - 1) * xSep / 2
        val currentStrenght = cardList.filter { !it.ist(Tag.SPECIAL) }.sumBy { it.currentStrength }
        cardList.toMutableList().map { master.getActor(it) }.forEachIndexed { i, cardActor ->
            cardActor.setNewPos(delta + i * xSep, y, if (isCathing()) 0.1 else 1.0)
            cardActor.refresh()
            cardActor.parentLine = this
        }
        strTx.text = "$baseText \n  Str:$currentStrenght"
//
//
        lastSize = cardList.size

    }

    var hoverScale = 1.0


    init {
        master.stage.addChild(this)
        refersh()
        onOut {
            hoverScale = 1.0
        }
        onOver {
            hoverScale = 1.1
        }
        onClick {
            laterGetter?.set(this@LineActor.location)
        }
        addUpdater {
            var sc = 1.0
            when {
                isCathing() -> {
                    sc = 1.0
                    val mf = master.mouseFollower
                    if (mf != null) {
                        val dif = y - master.stage.mouseY
                        val currentList = if (abs(dif) < ySep / 2) {
                            location.positionId =
                                ((master.stage.mouseX - delta) / xSep).roundToInt().coerceIn(0, list.size)
                            val l = list.toMutableList()
                            l.add(location.positionId, mf.card)
                            l
                        } else
                            list

                        if (currentList != lastList) {
                            generateCards(currentList)
                            lastList = currentList
                        }
                    }
                    im.bitmap = backBitmap.slice()
                }
                laterGetter != null -> {
                    sc = hoverScale
                    im.bitmap = backBitmap.slice()
                }
                laterGetter != null -> scale(hoverScale)
                else -> {
                    sc = 1.0
                    im.bitmap = frontBitmap.slice()
                }
            }
            scaleX = sc * (0.5 + lastSize)
            scaleY = sc
            strTx.alignRightToLeftOf(im)
            strTx.centerYOn(im)
        }
    }

    var lastList = listOf<Card>()
}