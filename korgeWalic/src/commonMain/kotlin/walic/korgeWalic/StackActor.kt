package walic.korgeWalic


import com.soywiz.kmem.divCeil
import com.soywiz.korge.input.onClick
import com.soywiz.korge.input.onOut
import com.soywiz.korge.input.onOver
import com.soywiz.korge.view.*
import walic.pureEngine.Card


const val midHidden = 1800.0
const val midExposed = 1200.0
const val exposedY = 800.0
const val maxCardPerStack = 5

class StackActor(private val master: RubeGoldbergRenderer, val getAllCards: () -> List<CardActor>, private val baseText: String, var defY: Double) : CardGroupAdapter() {
    val niceRect = NiceRect(master)
    var isHidden = true

    constructor(master: RubeGoldbergRenderer, list: List<Card>, baseText: String, defY: Double) : this(
        master,
        { list.toMutableList().map { master.getActor(it) } },
        baseText,
        defY
    )

    var listCardActor = listOf<CardActor>()
    fun refersh() {
        listCardActor = getAllCards()

        listCardActor.forEachIndexed { i, cardActor ->
            if (isHidden)
                cardActor.setNewPos(x, y, 0.5)
            else {
                cardActor.setNewPos(
                    midExposed + i % maxCardPerStack * xSep,
                    exposedY - ySep * (i / maxCardPerStack),
                    0.5
                )
            }
            cardActor.refresh()
            master.stage.removeChild(cardActor)
            master.stage.addChild(cardActor)
            cardActor.parentLine = this
        }
        if (isHidden) {
            master.stage.children.asSequence().filterIsInstance<Bullet>().map { listOf(it.target, it.firerer) }
                .flatten().distinct().filter { listCardActor.contains(it) }.forEachIndexed { i, cardActor ->
                    cardActor.setNewPos(x - (i + 2.5) * xSep, y, 0.1)
                }
            master.stage.removeChild(this)
            master.stage.addChild(this)
            niceRect.setNewPos(x, y, .5, .5, 0.5)
        } else {
            val h = listCardActor.size.divCeil(maxCardPerStack).toDouble()
            val w = listCardActor.size.coerceAtMost(maxCardPerStack).toDouble()
            niceRect.setNewPos(midExposed + (w - 1) / 2 * xSep, exposedY - (h - 1) / 2 * ySep, w, h, 0.5)
        }
    }

    var hoverScale = 1.0

    fun switchState() {
        isHidden = !isHidden
        if (isHidden) {
            master.openLine = null
        } else {
            master.openLine?.switchState()
            master.openLine = this
        }
        refersh()
    }

    init {
        image(frontBitmap) {
            width = defWidth
            height = defHeight
            x = -defWidth / 2
            y = -defHeight / 2
            text(baseText).apply {
                centerOn(this@image)
            }
        }
        master.stage.addChild(this)
        refersh()
        y = defY
        x = midHidden
        onClick {
            switchState()
        }
        onOut {
            hoverScale = 1.0
        }
        onOver {
            hoverScale = 1.1
        }
    }
}