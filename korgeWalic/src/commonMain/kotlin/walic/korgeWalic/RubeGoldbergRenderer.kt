package walic.korgeWalic


import com.soywiz.korge.view.*
import com.soywiz.korim.bitmap.Bitmap
import com.soywiz.korim.font.BitmapFont
import walic.pureEngine.*
import kotlin.math.sin
import kotlin.properties.Delegates


const val defHeight = 100.0
const val defWidth = 70.0

const val xSep = 100.0
const val ySep = 130.0

const val textY = 1200.0

const val leaderX = defWidth

var frontBitmap: Bitmap by Delegates.notNull()
var backBitmap: Bitmap by Delegates.notNull()
var font: BitmapFont by Delegates.notNull()

class RubeGoldbergRenderer(val stage: Stage) {
    val defShidt = ySep / 2

    var openLine: StackActor? = null


    inline fun appluFunction(f: () -> Unit) {
        f()
        refrehs()
    }

    var mouseFollower: CardActor? = null


    lateinit var state: State
    var playerId: Int = -1
    lateinit var sih: SecretInformationHandler
    fun setRequierd(state: State, playerId: Int, sih: SecretInformationHandler) {
        this.state = state
        this.playerId = playerId
        this.sih = sih
        createShit()
    }


    val mapCards = mutableMapOf<Long, CardActor>()

    fun getActor(card: Card): CardActor {
        return mapCards.getOrPut(card.uuid) {
            val index = state.players[playerId].knownHand.indexOf(card)
            if (index != -1) {
                val alternativeCard = sih.cardInHand[index]
                if (card == alternativeCard) {
                    println("Something Wrong with $card")
                    CardActor(this, card)
                } else
                    getActor(alternativeCard)
            } else
                CardActor(this, card)
        }
    }

    var bigCardTextActor: Text = stage.text("Big card") {
        position(textY, 2 * ySep)
    }

    var okOkButton: OkButton = OkButton(this).apply {
        position(midHidden, defShidt)
    }

    var messageText: List<Text> = (0..10).map { stage.text("") }.apply {
        forEachIndexed { index, text -> text.position(textY, (5 - index / 4f) * ySep + defShidt) }
    }
    private var playerInfo: List<Text> = listOf(stage.text(""), stage.text(""))
    var time = 0.0
    var osciScale = 1.1

    init {
        playerInfo[0].position(leaderX + defWidth / 2, ySep * 1.5)
        playerInfo[1].position(leaderX + defWidth / 2, ySep * 6.5)
        stage.addUpdater {
            time += it.seconds
            osciScale = (sin(time) + 10) / 10
        }

    }


    lateinit var otherCards: StackActor
    private lateinit var yourDeck: StackActor
    private lateinit var graviards: List<StackActor>
    private lateinit var banished: StackActor

    private lateinit var enemyDeck: StackActor
    val enemyDeckCards = mutableListOf<Card>()

    val otherCardsList = mutableListOf<CardActor>()


    lateinit var enemyHand: LineActor
    lateinit var lines: List<LineActor>
    private lateinit var yourHand: LineActor


    fun createShit() {
        println("Creating shit")

        val lists = mutableListOf<Pair<List<Card>, String>>(Pair(sih.cardInHand, "Your\nHand"))
        val names = listOf("Support", "Range", "Attack")

        lists.addAll(state.players[playerId].rows.mapIndexed { i, list -> Pair(list, "Your\n${names[i]}") })
        lists.addAll(
            state.players[1 - playerId].rows.reversed().mapIndexed { i, list -> Pair(list, "Enemy\n${names[2 - i]}") })
        lists.add((Pair(state.players[1 - playerId].knownHand, "Enemy\nHand")))

        val tempLine = lists.mapIndexed { index, pair ->
            LineActor(this, pair.first, pair.second, ySep * index + defShidt)
        }

        lines = tempLine.drop(1).dropLast(1)
        lines.forEachIndexed { index, lineActor ->
            if (index < 3)
                lineActor.location = CardLocation(playerId, index)
            else
                lineActor.location = CardLocation(1 - playerId, 2 - index % 3)

        }
        yourHand = tempLine[0]
        enemyHand = tempLine.last()

        graviards = state.players.mapIndexed { index, player ->
            StackActor(
                this,
                player.graveyard,
                "Graveyard",
                defShidt + ySep * (if (index == playerId) 1 else 6)
            )
        }
        otherCards = StackActor(this, { otherCardsList }, "Other Cards", defShidt + ySep * 2)
        yourDeck = StackActor(this, sih.cardToDraw, "Your Deck", defShidt + ySep * 3)
        banished = StackActor(
            this,
            { state.players.map { it.banished }.flatten().map { getActor(it) } },
            "Banished",
            defShidt + ySep * 4
        )
        enemyDeck = StackActor(this, enemyDeckCards, "Enemy Deck", defShidt + ySep * 7)
    }


    fun refrehs() {

        mapCards.values.forEach {
            it.additonalText = ""
            it.parentLine = null
            it.setNewPos(midHidden, defShidt + 5 * ySep, 1.0)
        }

        this.stage.children.filterIsInstance<LineActor>().forEach { it.refersh() }

        val good = this.stage.children.filterIsInstance<StackActor>().map { it.refersh();it }
            .filter { stackActor -> stackActor.listCardActor.any { it.pickableIndex != null } && stackActor.isHidden }
        if (good.size == 1) {
            good[0].switchState()
        }


        state.players.map { it.rows zip it.weathers }.flatten().forEach { (list, wether) ->
            lines.find { it.list == list }?.wetherText =
                if (wether != null)
                    wether::class.simpleName!!.camelToHumanRedable().replace(" ", "\n")
                else
                    "" //wolne ale jebac
        }

        state.players.forEachIndexed { index, player ->
            if (player.leader != null) {
                val cardActor = getActor(player.leader!!)
                cardActor.setNewPos(leaderX, defShidt + ySep * if (index == playerId) 0 else 7, 1.0)
                cardActor.refresh()
            }
            playerInfo[if (index == 0) playerId else 1 - playerId].text =
                "Strength: ${player.getStrenght()}\n Wins:${player.wins}\n Passed: ${player.didPass}\n"
        }
        state.players[playerId].knownHand.forEach { getActor(it).additonalText = if (it.ac !is UnknowCard) "R" else "" }
        sih.cardAtBottom.forEachIndexed { index, card -> getActor(card).additonalText = "B$index" }
        sih.cardAtTop.forEachIndexed { index, card -> getActor(card).additonalText = "T$index" }
    }


    fun spawnCardOnSide(card: Card, owner: Int) {
        getActor(card).position(midHidden, ySep * if (owner == playerId) 3 else 7)
    }
}
