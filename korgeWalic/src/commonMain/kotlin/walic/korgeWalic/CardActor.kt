package walic.korgeWalic

import com.soywiz.klock.milliseconds
import com.soywiz.klock.seconds
import com.soywiz.korge.input.MouseEvents
import com.soywiz.korge.input.onClick
import com.soywiz.korge.input.onOut
import com.soywiz.korge.input.onOver
import com.soywiz.korge.view.*
import com.soywiz.korim.bitmap.slice
import com.soywiz.korim.color.Colors
import com.soywiz.korio.async.delay
import com.soywiz.korio.async.launchImmediately
import com.soywiz.korma.geom.Point
import walic.pureEngine.Card
import walic.pureEngine.UnknowCard
import walic.pureEngine.asFanyText
import walic.pureEngine.switch


import kotlin.math.PI
import kotlin.math.cos


class CardActor(private val master: RubeGoldbergRenderer, var card: Card) : Container() {

    var parentLine: CardGroupAdapter? = null

    var currentBitmap = frontBitmap
    var longText = ""
    var additonalText = ""

    private val im = image(currentBitmap) {
        width = defWidth
        height = defHeight
        x = -defWidth / 2
        y = -defHeight / 2
    }
    private val tx = text("", 24.0, Colors.WHITE, font)

    var hoverScale = 1.0
    var rotationScale = 1.0

    init {
        refresh()
        addChild(im)
        addChild(tx)

        val oc: suspend (MouseEvents) -> Unit = {
            println("Click ${this@CardActor}")
            if ((this@CardActor.parentLine as? StackActor)?.isHidden != true)
                pickableIndex?.let { master.okOkButton.pickedCards.switch(it) }
            (this@CardActor.parentLine as? LineActor)?.let { it.laterGetter?.set(it.location) }
        }
        onClick(oc)
        tx.onClick(oc)
        onOver {
            hoverScale = 1.2
            master.bigCardTextActor.text = longText
        }
        onOut {
            hoverScale = 1.0
            if (master.bigCardTextActor.text == longText)
                master.bigCardTextActor.text = ""
        }
        addUpdater {
            val timeScale =
                if ((pickableIndex != null)) {
                    master.osciScale
                } else
                    1.0
            val stateScale = if (master.okOkButton.pickedCards.contains(pickableIndex))
                0.7
            else 1.0

            scaleX = hoverScale * stateScale * timeScale * rotationScale
            scaleY = hoverScale * stateScale * timeScale
            if (newPos != null) {
                totalTime += it / (defMovemntTime * newTimeMult)
                totalTime = totalTime.coerceAtMost(1.0)
                this.pos = newPos!! * totalTime + oldPos!! * (1 - totalTime)
            }
        }
        refresh()
        master.stage.addChild(this)

    }


    fun refresh() {
        tx.text = (if (card.ac is GuiAdapter.ButtonCard) card.ac.description else card.toString()).replace(" ", "\n") + additonalText
        longText = card.asFanyText().joinToString("\n")
        tx.centerBetween(-defWidth / 2, -defHeight * 0.8, defWidth / 2, 0.0)
        im.bitmap = if (card.ac is UnknowCard)
            backBitmap.slice()
        else
            frontBitmap.slice()


    }

    private val defMovemntTime = 0.3.seconds

    var oldPos: Point? = null
    var newPos: Point? = null
    var totalTime = 0.0
    var newTimeMult = 1.0

    fun setNewPos(newX: Double, newY: Double, time: Double = 1.0) {
        totalTime = 0.0
        oldPos = Point(this.x, this.y)
        newPos = Point(newX, newY)
        newTimeMult = time
    }

    //pick on of cards
    var pickableIndex: Int? = null

    val timeScale = 0.2.seconds
    fun reveal(new: Card): Double {
        if (card.uuid == new.uuid && card.ac == new.ac) {
            this.card = new
            return 0.0
        }
        master.stage.launchImmediately {
            var time = 0.0.milliseconds
            val dt = 16.milliseconds

            while (time < timeScale * PI / 2) {
                time += dt
                rotationScale = cos(time / timeScale)
                delay(dt) // suspending
            }
            this.card = new
            refresh()
            while (time < timeScale * PI) {
                time += dt
                rotationScale = -cos(time / timeScale)
                delay(dt) // suspending
            }
            rotationScale = 1.0
            this.card = new
            refresh()
        }
        return timeScale * 2 * PI / 1.0.seconds
    }

}