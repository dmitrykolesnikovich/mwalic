import com.soywiz.klock.*
import com.soywiz.korge.*
import com.soywiz.korge.input.onClick
import com.soywiz.korge.ui.uiButton
import com.soywiz.korge.view.*
import com.soywiz.korim.color.*
import com.soywiz.korim.font.readBitmapFont
import com.soywiz.korim.format.*
import com.soywiz.korio.async.launch
import com.soywiz.korio.async.launchImmediately
import com.soywiz.korio.file.std.*
import com.soywiz.korio.lang.toByteArray
import com.soywiz.korio.net.AsyncClient
import com.soywiz.korio.net.AsyncServer
import com.soywiz.korio.stream.readString
import com.soywiz.korio.stream.writeString
import com.soywiz.korma.geom.*
import kotlinx.coroutines.channels.Channel
import walic.gwentBeta.initGwentBeta
import walic.korgeWalic.*
import walic.pureEngine.RadnomAdapter
import walic.pureEngine.State
import walic.pureEngine.SteamAdapter

import kotlin.random.Random


suspend fun runLocal(deck: List<String>?, stage: Stage, hostname: String?) {
    stage.children.forEach { it.x += 100000;it.y += 100000 }

    val rubeGoldbergRenderer = RubeGoldbergRenderer(stage)
    val port = 1234

    val (inS: suspend (String) -> Unit, outS: suspend () -> String, playerId) = when (hostname) {
        null -> {
            val streams = listOf(Channel<String>(10), Channel(10))
            val s2 = State(SteamAdapter(streams[0]::receive, streams[1]::send, 1, RadnomAdapter(true, Random), 0))
            stage.launchImmediately {
                s2.mainLoop()
            }
            Triple<suspend (String) -> Unit, suspend () -> String, Int>(streams[0]::send, streams[1]::receive, 0)
        }
        "serwer" -> {
            println("Starting as server at port:$port")
            val socket = AsyncServer(port).accept()
            Triple<suspend (String) -> Unit, suspend () -> String, Int>(
                { socket.write(it.length);socket.writeString(it) },
                { socket.readString(socket.read()) },
                0
            )
        }
        else -> {
            println("Starting with host:$hostname and port:$port")
            val socket = AsyncClient(hostname, port)
            Triple<suspend (String) -> Unit, suspend () -> String, Int>(
                { socket.write(it.length); socket.writeString(it) },
                { socket.readString(socket.read()) },
                1
            )
        }
    }

    stage.launch {
        State(SteamAdapter(outS, inS, playerId, GuiAdapter(rubeGoldbergRenderer, deck), debugLevel = 0)).mainLoop()

    }

}

suspend fun runTest(stage: Stage) {
    val startTime = getMiliseconds()
    (0 until 1).map { tId ->
        stage.launchImmediately {
            while (true) {
                val streams = listOf(Channel<String>(10), Channel(10))
                listOf(0, 1).map { p ->
                    stage.launchImmediately {
                        (0..Int.MAX_VALUE).forEach { inter ->
                            val randomSeed = Random.nextLong()
                            val adapter = RadnomAdapter(false, Random(randomSeed))
                            val steamAdapter =
                                SteamAdapter(streams[p]::receive, streams[1 - p]::send, p, adapter, 0)
                            try {
                                State(steamAdapter).mainLoop()
                            } catch (e: Exception) {
                                println("Random seeed was $randomSeed for $p")
                                adapter.log.forEach { println("$p -> $it") }
                                userHomeVfs["replies/crushDump$p-$inter.wrp"].write(
                                    steamAdapter.getReplay().toByteArray()
                                )
                                streams[1 - p].send("Some random value that will crash that other stream Xd no the best idea but whatever")
                                streams[1 - p].send("}[} one more time to be sure")
                                throw e
                            }

                            if (p == 0 && tId == 0)
                                println("Iter $inter (${1.0 * inter / (getMiliseconds() - startTime) * 1000})")

                        }
                    }
                }.forEach { it.join() }
            }
        }
    }.forEach { it.join() }
}

suspend fun creatButtons(
    list: List<Pair<String, suspend () -> List<String>?>>,
    stage: Stage,
    hostname: String?,
    index: () -> Int
) {
    list.forEach { (name, function) ->
        stage.uiButton {
            text(name).centerOn(this)
            val i = index()
            position(1000 + 300 * (i / 5), 200 + 100 * (i % 5))
            onClick {
                runLocal(function(), stage, hostname)
            }

        }
    }
}


suspend fun main(args: Array<String> = listOf<String>().toTypedArray()) =
    Korge(width = 1920, height = 1080, bgcolor = Colors["#2b2b2b"]) {
        initGwentBeta()
        var index = 0
        frontBitmap = resourcesVfs["Card.png"].readBitmap()
        backBitmap = resourcesVfs["CardBack.png"].readBitmap()
        font = resourcesVfs["clear_sans.fnt"].readBitmapFont()


        var c: Channel<String>? = null
        stage.launch {
            while (true) {
                c = Channel()
                println(c?.receive())
                c = null
            }
        }
        var i = 0
        stage.launch {
            stage.uiButton {
                text("Send").centerOn(this)
                position(500, 200)
                onClick {
                    c?.send("lol${i++}")
                }
            }
        }





        uiButton {
            text("Run test").centerOn(this)
            position(700, 200)
            onClick {
                runTest(this@Korge)
            }
        }
        creatButtons(
            listOf(Pair<String, suspend () -> List<String>?>("Random", { null })),
            this@Korge,
            args.elementAtOrNull(0)
        ) { index++ }

        creatButtons(
            listOf(
                "alchemy.wcd",
                "armor.wcd",
                "cursed.wcd",
                "discard.wcd",
                "machines.wcd",
                "reveal.wcd",
                "spies.wcd"
            ).map { s ->
                Pair("def ->$s ",
                    {
                        resourcesVfs["Decks"][s].readAll().decodeToString().split("\n").drop(1)
                            .filter { it.isNotBlank() }
                    })
            },
            this@Korge,
            args.elementAtOrNull(0)
        ) { index++ }

        uiButton {
            text("Load decks").centerOn(this)
            position(700, 400)
            onClick {
                creatButtons(
                    userHomeVfs["Decks"].listNames().map { s ->
                        Pair<String, suspend () -> List<String>?>(
                            "loaded-> $s", {
                                userHomeVfs["Decks"][s].readAll().decodeToString().split("\n")
                                    .filter { it.isNotBlank() && it.first() != "#"[0] }
                            })
                    },
                    this@Korge,
                    args.elementAtOrNull(0)
                ) { index++ }
            }
        }
    }

