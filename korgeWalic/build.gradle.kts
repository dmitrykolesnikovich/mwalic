import com.soywiz.korge.gradle.*

buildscript {
	repositories {
        jcenter() 
		mavenLocal()
		maven { url = uri("https://dl.bintray.com/korlibs/korlibs") }
		maven { url = uri("https://plugins.gradle.org/m2/") }
		mavenCentral()
		google()
	}
	dependencies {
		classpath("com.soywiz.korlibs.korge.plugins:korge-gradle-plugin:2.0.0.0")
	}
}


apply(plugin = "com.soywiz.korge")


korge {
	id = "walic.walicKorge"
	targetJvm()
//	targetJs()
//    targetAndroid()
//	targetDesktop()
	dependencies {
		add("commonMainApi", "org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")
		add("commonMainApi", "org.jetbrains.kotlin:kotlin-reflect:1.4.20")
		add("commonMainApi",(project(":pureEngine")))
		add("commonMainApi",(project(":gwentBeta")))
	}
}
