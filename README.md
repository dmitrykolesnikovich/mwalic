# What is this
This is an unofficial open source copy of the last version of gwent beta. Right now there are only 3 of 5 fractions ready and multiple bugs but whatever.
![screenshot in game](Img/game.png)
# How to run this
You need to download artefacts of the latest build, there you will find desktop-1.0-jar. If you want to play with other players(and not with random-moves-making computer ) one of you need to redirect ports 2137 and 2138 to his device and run as “serwer” , the other on has to type ip of the host.
# Can you cheat?
You should not be able to. If you do game should detect it and inform other player about it after the game is over.
# Legal 
This project, libGdx, korge and korIO  are licensed under  Apache License 2.0. You can find text in LICENSE text file. 
No affiliation with CD Projekt Red. 
