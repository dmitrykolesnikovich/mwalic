package walic.libgdxWalic


import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction
import walic.pureEngine.Card
import walic.pureEngine.CardLocation
import walic.pureEngine.Tag
import kotlin.math.abs

import kotlin.math.roundToInt

const val midNone = 800f


class LibGdxLineActor(private val master: LibGdxRenderer, val list: List<Card>, private val baseText: String) : LibGdxCardsHandeler() {

    var location = CardLocation(-1, -1, -1)
    var function: (() -> Unit)? = null
    var wetherText = ""

    enum class Mods {
        None,
        CatchCards,
        Select,
    }

    var mod = Mods.None

    override fun refersh() {
        generateCards(list)
    }

    private var currentStrenght = 0

    var deltaX = 0f
    private fun generateCards(cardList: List<Card>) {
        if (master.mouseFollower?.controller == this)
            master.mouseFollower!!.controller = null

        setXSize(cardList.size + defWidth / (2 * xSep))
        deltaX = x - cardList.size * xSep / 2 + defWidth
        currentStrenght = cardList.filter { !it.ist(Tag.SPECIAL) }.sumBy { it.currentStrength }
        setPosition(-width / 2 + midNone, y)
        cardList.toMutableList().map { master.getActorOrCreate(it) }.forEachIndexed { i, cardActor ->
            if (mod == Mods.CatchCards)
                cardActor.setPosWithAniamtion(deltaX + i * xSep, y + (width - defWidth) / 2, 0.1f)
            else
                cardActor.setPosWithAniamtion(deltaX + i * xSep, y + (width - defWidth) / 2)
            cardActor.refresh()
            cardActor.controller = this
        }
    }

    init {
        width = xSep
        height = defHeight * 1.3f
        originX = width / 2
        originY = height / 2

        master.stage.addActor(this)
        refersh()

        addListener(object : InputListener() {
            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (event!!.isHandled)
                    return true
                event.handle()
                if (function != null) {
                    function!!()
                }
                return true
            }
        })
    }

    var lastList = listOf<Card>()
    var rescaleAction: ScaleToAction? = null
    fun setXSize(newScale: Float) {
        this.removeAction(rescaleAction)
        rescaleAction = Actions.scaleTo(newScale, this.scaleY, 0.5f)
        this.addAction(rescaleAction)
    }

    override fun act(delta: Float) {
        when (mod) {
            Mods.None -> {
            }
            Mods.CatchCards -> {
                val mf = master.mouseFollower
                if (mf != null) {
                    val pos = master.getMouseCordinates()
                    val dif = y - pos.y + height / 2
                    val currentList = if (abs(dif) < ySep / 2) {
                        location.positionId = ((pos.x - deltaX - xSep / 2) / xSep).roundToInt().coerceIn(0, list.size)
                        val l = list.toMutableList()
                        l.add(location.positionId, mf.card)
                        l
                    } else
                        list
                    if (currentList != lastList) {
                        generateCards(currentList)
                        lastList = currentList
                    }
                }
            }
            Mods.Select -> {
            }
        }
        super.act(delta)
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        val color: Color = color
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)
        val image = when (mod) {
            Mods.None -> cardFront
            Mods.CatchCards, Mods.Select -> cardBack
        }
        batch.draw(image, x, y, originX, originY, width, height, scaleX, scaleY, rotation)
        val csText = "Strength $currentStrenght "

        standardFont.draw(batch, "$baseText\n  $csText", x - width * scaleX / 2 - defWidth / 2, y + defHeight * 1f, defWidth, 1, true)
        standardFont.draw(batch, wetherText, x + width * scaleX / 2 + defWidth / 2, y + defHeight * 0.7f, defWidth, 1, true)
        super.draw(batch, parentAlpha)
    }
}