package walic.libgdxWalic

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import walic.libgdxWalic.Bullet
import walic.pureEngine.Card


const val midHidden = 1800f
const val exposedX = 1200f
const val exposedY = 500f
const val maxPerRow = 5

class LibGdxStackActor(private val master: LibGdxRenderer, val list: () -> List<Card>, val baseText: String, defY: Float) : LibGdxCardsHandeler() {

    var isHidden = true
    var listCardActor = listOf<CardActor>()
    override fun refersh() {
        listCardActor = list().toMutableList().map { master.getActorOrCreate(it) }.filter { it.controller == null || it.controller==this}
        listCardActor.forEachIndexed { i, cardActor ->
            cardActor.controller = this
            if (isHidden) {
                cardActor.setPosWithAniamtion(x, y, 0.5f)
            } else
                cardActor.setPosWithAniamtion(exposedX + (i % maxPerRow) * defWidth, exposedY + (i / 5) * defHeight)
            cardActor.refresh()
        }
        this.zIndex = Int.MAX_VALUE
        master.stage.actors.asSequence().filterIsInstance<Bullet>().map { listOf(it.end, it.start) }.flatten().distinct().filter { it in listCardActor }.filterIsInstance<CardActor>().forEachIndexed { i, cardActor ->
            cardActor.setPosWithAniamtion(-(i + 2) * xSep + x, y, 0.5f)
        }
    }

    var hoverScale = 1f

    fun switchState() {
        isHidden = !isHidden
        if (isHidden) {
            master.openLibGdxStack = null
        } else {
            zIndex = 1000
            master.openLibGdxStack?.switchState()
            master.openLibGdxStack = this
        }
        refersh()
    }

    init {
        width = defWidth
        height = defHeight
        x = midHidden
        y = defY
        master.stage.addActor(this)
        refersh()

        addListener(object : InputListener() {
            override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                hoverScale = 1.1f
            }

            override fun exit(event: InputEvent?, x: Float, y: Float, pointer: Int, toActor: Actor?) {
                hoverScale = 1f
            }

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (event!!.isHandled)
                    return true
                event.handle()
                switchState()
                return true
            }
        })
    }

    private fun getScale(): Float {
        return when {
            isHidden && listCardActor.any { it.function != null } -> hoverScale * master.timeOscScale
            else -> hoverScale
        }
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        super.draw(batch, parentAlpha)
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)
        batch.draw(if (isHidden) cardFront else cardBack, x, y, originX, originY, width * getScale(), height * getScale(), scaleX, scaleY, rotation)
        standardFont.draw(batch, "$baseText\n ", x - defWidth, y + defHeight * 0.7f, defWidth, 1, true)

    }

}