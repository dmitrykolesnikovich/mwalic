package walic.libgdxWalic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction


import walic.pureEngine.Card
import walic.pureEngine.UnknowCard
import walic.pureEngine.asFanyText
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

const val timeItTakesToReveal = 0.5f
var cardActorIdCount = 0

class CardActor(private val master: LibGdxRenderer, var card: Card) : Actor() {
    val id = cardActorIdCount++
    var function: (() -> Unit)? = null
    var hoverScale = 1f
    var stateScale = 1f
    var text = ""
    var longText = ""
    var controller: LibGdxCardsHandeler? = null

    private fun isActive(): Boolean {
        return true
    }

    fun refresh() {
        text = (if (card.ac !is LibGdxAdapter.ButtonCard) card.toString() else card.ac.description)
        longText = card.asFanyText().joinToString("\n") + "\nId:$id/${card.uuid}"
    }

    //tryToGetActor?
    fun getAlternativeIndex(): Long? {
        val index = master.sih.cardInHand.indexOf(card)
        if (index == -1)
            return null
        return master.state.players[master.playerId].knownHand.elementAtOrNull(index)?.uuid
    }

    init {
        println("Creaing for $card $id")
        master.stage.addActor(this)
        x = 500f
        y = 500f
        setSize(defWidth, defHeight)
        originX = defWidth / 2
        originY = defHeight / 2
        addListener(object : InputListener() {
            override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                hoverScale = 1.2f
                master.bigCardTextActor.setText(longText)

                master.outputQueue.offer(getAlternativeIndex() ?: this@CardActor.card.uuid)
            }

            override fun exit(event: InputEvent?, x: Float, y: Float, pointer: Int, toActor: Actor?) {
                hoverScale = 1f
                master.bigCardTextActor.setText("")
                master.outputQueue.offer(Long.MAX_VALUE)
            }

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (event!!.isHandled)
                    return true
                if (!isActive())
                    return false
                if (function != null) {
                    function!!.invoke()
                }
                return true
            }

        })
        refresh()
    }


    private val interpol = Interpolation.pow2
    override fun act(delta: Float) {
        revelTimer += delta
        val timeScale =
            if (function != null && isActive())
                master.timeOscScale
            else
                1f

        val enemyHoverScale = if (card.uuid == master.currentHover || (master.currentHover != null && getAlternativeIndex() == master.currentHover)) 1.1f else 1f
        setScale(timeScale * hoverScale * stateScale * enemyHoverScale)
        super.act(delta)
    }


    var moveAction: MoveToAction? = null
    fun setPosWithAniamtion(xTs: Float, yTs: Float, moveTime: Float = 0.5f) {
        removeAction(moveAction)
        moveAction = Actions.moveTo(xTs, yTs, moveTime, interpol)
        addAction(moveAction)
        master.stage.addActor(this)


    }


    private var oldCard: Card? = null
    private var oldText = ""
    private var revelTimer = 0f
    fun reveal(new: Card) {
        if (card.name != new.name) {
            revelTimer = -timeItTakesToReveal
        }
        oldCard = card
        card = new
        oldText = text
        refresh()
    }


    var reveled = false
    override fun draw(batch: Batch, parentAlpha: Float) {
        val color: Color = color
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)

        val image = if (card.ac is UnknowCard || reveled || revelTimer < -timeItTakesToReveal / 2)
            cardBack
        else
            imageMap.getOrDefault(card.ac, cardFront)

        val xscale = if (revelTimer < 0) abs(cos(revelTimer / timeItTakesToReveal * Math.PI)).toFloat() else 1f
        val xshift = if (revelTimer < 0) -width / 2 * sin(revelTimer / timeItTakesToReveal * Math.PI).toFloat() else 0f
        batch.draw(image, x + xshift, y, originX, originY, xscale * width, height, scaleX, scaleY, rotation)
        if (revelTimer > -timeItTakesToReveal / 2)
            standardFont.draw(batch, text, x, y + defHeight * 0.7f, defWidth, 1, true)

    }
}