package walic.libgdxWalic

import ktx.scene2d.*
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import ktx.actors.onChange
import ktx.actors.onClick
import walic.gwentBeta.initGwentBeta
import walic.pureEngine.*
import java.io.FileWriter
import java.time.LocalDateTime
import kotlin.random.Random
import kotlin.random.nextUInt


class DeckCreator(stage: Stage,val master: InitialRenderer) : Actor() {


    init {
        stage.clear()
        initGwentBeta()
    }

    val betterAll = allNormalCards.sortedWith(compareBy(AbstractCard::getMaxInInnitialDeck, AbstractCard::name)).map { Pair(it, it.asFanyText().joinToString("\n")) }
    val searchFiled = TextField("", defSkin)
    val leaderBox = scene2d.selectBox<AbstractCard>(skin = defSkin) {
        items = com.badlogic.gdx.utils.Array(allLeaders.toTypedArray())
        onChange {
            printDeck()
            setSaveText()
        }
    }
    val cardsPanel = scene2d.table(skin = defSkin)
    val deckCardSummary = Label("", defSkin)
    val loadPickler = SelectBox<DeckInfo>(defSkin)
    val saveName = TextField("", defSkin)
    val statusBox = Label("", defSkin)
    val deckPanel = Table()
    var currentDeck = mutableListOf<AbstractCard>()

    init {
        scene2d.table {
            width = stage.width
            height = stage.height
            stage.addActor(this)
            table(defSkin) {
                height = stage.height
                table {
                    add(searchFiled)
                    add(leaderBox)
                    textButton("Exit",skin = defSkin){
                        onClick {
                            stage.clear()
                            master.create()
                        }
                    }
                }
                row()
                scrollPane(skin = defSkin) {
                    actor = cardsPanel
                    height = stage.height * 0.8f
                }
            }
            table(skin = defSkin) {
                width = stage.width / 2
                height = stage.height
                table {
                    textButton("Save", skin = defSkin) {
                        onClick {
                            val name = saveName.text
                            val writer = FileWriter(getDeckPatch().resolve(name))
                            writer.write("# deck created @ ${LocalDateTime.now()}\n")
                            writer.write(leaderBox.selected.name + "\n")
                            for (card in currentDeck) {
                                writer.write(card.name + "\n")
                            }
                            writer.close()
                            refershFiles()
                        }
                    }
                    add(saveName).width(400f)

                }
                row()
                table {
                    textButton("Load", skin = defSkin) {
                        onClick {
                            val rf = loadPickler.selected?.content ?: return@onClick
                            val pair = praseDeck(rf) { 0 }
                            leaderBox.selectedIndex = allLeaders.indexOfFirst { pair.first.name == it.name }
                            currentDeck = pair.second.map { it.ac }.toMutableList()
                            printDeck()
                            saveName.text= loadPickler.selected.name.split(" ").last()
                        }
                    }
                    add(loadPickler)
                    textButton("Refersh", skin = defSkin) {
                        onClick {
                            refershFiles()
                        }
                    }
                }
                row()
                table {
                    add(statusBox)
                }
                row()
                scrollPane(skin = defSkin) {
                    actor = deckPanel
                    height = stage.height / 2
                }
                row()
                scrollPane(skin = defSkin) {
                    actor = deckCardSummary
                    height = stage.height / 4
                }
            }
        }
    }
    var last = "Not black"
    fun doSearch() {
        if( searchFiled.text==last)
            return
        last =  searchFiled.text
        cardsPanel.clear()
        val split = searchFiled.text.split(" ")
        val fraction = leaderBox.selected.fraction!!
        var count = 0
        betterAll.filter { (ac, fancy) -> ac.isFraction(fraction) && (split.isEmpty() || split.all { fancy.contains(it, ignoreCase = true) }) }.forEach { (ac, fancy) ->
            val singleCardPanel = Table()
            val label = Label(fancy, defSkin)
            label.setAlignment(Align.left)
            singleCardPanel.add(label)
            if ((count++) % 2 == 0) {
                cardsPanel.row()
            }

            val addButton = TextButton("Add 1", defSkin)
            addButton.onClick {
                currentDeck.add(ac)
                printDeck()
            }
            val addMax = TextButton("Add max", defSkin)
            addMax.onClick {
                repeat(ac.getMaxInInnitialDeck()-currentDeck.count{it==ac}) {
                    currentDeck.add(ac)
                }
                printDeck()
            }
            singleCardPanel.row()
            val table = Table()
            table.add(addButton)
            table.add(addMax)
            singleCardPanel.add(table)
            cardsPanel.add(singleCardPanel)
        }
    }
    class DeckInfo(val name:String, val content:List<String>){
        override fun toString(): String {
            return name
        }
    }

    fun refershFiles() {
        val lol = getPossibleDecks().filter { it.second!=null }.map { DeckInfo(it.first,it.second!!) }
        loadPickler.items = com.badlogic.gdx.utils.Array((lol).toTypedArray())
    }

    fun setSaveText() {
        saveName.text = "${leaderBox.selected.getNiceName().replace(" ", "")}-${Random.nextUInt()}.wcd"
    }

    private fun printDeck() {
        deckPanel.clear()
        currentDeck = currentDeck.sortedWith(compareBy(AbstractCard::getMaxInInnitialDeck, AbstractCard::name)).toMutableList()
        (listOf(leaderBox.selected) + currentDeck).groupBy { it.name }.forEach { (_, list) ->
            val singleCardPanel = Table()
            val label = Label(list.first().getNiceName() + " x${list.size}/${list.first().getMaxInInnitialDeck()}", defSkin)
            label.addListener(object : InputListener() {
                override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                    deckCardSummary.setText(list.first().asFanyText().joinToString("\n"))
                }
            })
            singleCardPanel.add(label)

            val removeOne = TextButton("Remove 1", defSkin)
            removeOne.onClick {
                currentDeck.remove(list.first())
                printDeck()
            }
            singleCardPanel.add(removeOne)

            val removeALL = TextButton("Remove All", defSkin)
            removeALL.onClick {
                currentDeck.removeAll(list)
                printDeck()
            }
            singleCardPanel.add(removeALL)

            deckPanel.add(singleCardPanel)
            deckPanel.row()
        }
        val iniialmMess = try {
            checkIfDeckOk(leaderBox.selected as AbstractCard, currentDeck)
            "OK"
        } catch (e: WrongInputException) {
            e.message
        }
        statusBox.setText("$iniialmMess  Gold ${currentDeck.count { it.ist(Tag.Gold) }} / $maxGold , Silver  ${currentDeck.count { it.ist(Tag.Silver) }} / $maxSilver")
    }

    init {
        doSearch()
        printDeck()
        refershFiles()
        setSaveText()
        stage.addActor(this)
    }

    override fun act(delta: Float) {
        doSearch()
        super.act(delta)
    }
}
