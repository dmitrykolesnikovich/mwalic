package walic.jvmWalic


import walic.pureEngine.*
import java.io.File
import kotlin.random.Random

class ConsolePlayerAdapter(val deck: String? = null) : PlayerAdapter() {
    //graphics
    lateinit var state: State
    lateinit var sih: SecretInformationHandler
    var playerId: Int = -1


    override fun setRequierd(state: State, noDeck: Boolean): Pair<List<String>, Long> {

        this.state = state
        this.sih = state.steamAdapter.secretInformationHandler
        this.playerId = state.steamAdapter.playerNumber
        val dir = System.getProperty("user.dir") + "/Decks"

        return Pair(
                when {
                    noDeck -> listOf()
                    deck == null -> {
                        val foundDecks = mutableListOf<File>()
                        println("Which deck do you want?")
                        File(dir).listFiles()!!.forEach {
                            if (it.isFile && it.extension == "wcd") {
                                println("${foundDecks.size}  Deck at \"${it.canonicalPath}\". Described:  ${it.readLines()[0]}")
                                foundDecks.add(it)
                            }
                        }
                        val deck = foundDecks[readLine()!!.toInt()]
                        readFile(deck)
                    }
                    else -> {
                        readFile(File("$dir/$deck"))
                    }
                }, Random.nextLong())
    }

    override suspend fun <A> pickFromList(list: List<A>, n: Int, canLess: Boolean): List<Int> {
        TODO("Not yet implemented")
    }


    private fun printState() {
        println("==============")
        state.players.forEachIndexed { index, player ->
            println("Player no $index @${player.getStrenght()}")
            player.rows.forEachIndexed { i, v ->
                print("Lane $i->")
                printWithIndex(v)
            }
            print("Graveyard-> ")
            printWithIndex(player.graveyard)
            print("Discarded-> ")
            printWithIndex(player.banished)
            print("known Hand->")
            printWithIndex(player.knownHand)
            println("==============")
        }
        println("Secret Info")
        print("Hand->")
        printWithIndex(sih.cardInHand)
        print("Deck->")
        printWithIndex(sih.cardToDraw)
    }


    private fun facnyCardPrint(card: Card) {
        println("========\n" + card.asFanyText().joinToString("\n") + "========\n")
    }

    private fun <A> readVal(question: String, praser: (String) -> A, checker: (A) -> Boolean): A {
        while (true) {
            println(question)
            val inputString = readLine()!!
            if (inputString == "info") {
                val allCards = state.players.map { it.rows.flatten() + it.graveyard + it.banished }.flatten() + sih.cardInHand + sih.cardToDraw
                printState()
                while (true) {
                    println("info about what?exit to exit")
                    val input = readLine()
                    if (input == "exit")
                        break
                    val card = allCards.find { it.name == input }
                    if (card == null) {
                        println("Wrong name")
                        continue
                    }
                    facnyCardPrint(card)
                }
            } else
                try {
                    val input = praser(inputString)
                    if (checker(input))
                        return input
                    println("Wrong input")
                } catch (e: Exception) {
                    println("Wrong - $e")
                }
        }
    }

    private fun printWithIndex(cards: List<Card?>) {
        cards.forEachIndexed { i, v -> print("$i:${v?.toString() ?: "Unknown"} ") }
        println()
    }


    override suspend fun pickRow(mask: RowMask): CardLocation {
        state.players.forEachIndexed { p: Int, player: State.Player ->
            println("Player $p")
            player.rows.forEachIndexed { r, row ->
                if (mask.check(playerId, p, r))
                    println("Lane $r:")
                else
                    println("Row forrbiden")
                printWithIndex(row)
            }
        }
        return readVal("Pick row by player row ", { ("$it 0").toCardLocation() }, { mask.check(playerId, it) })
    }

    override suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation {
        for (p in state.players.indices) {
            for (l in state.players[p].rows.indices) {
                print("$p $l->")
                if (mask.check(playerId, p, l)) {
                    print("p:$p l:$l->")
                    val thatLane = state.players[p].rows[l]
                    thatLane.forEachIndexed { i, v -> print("|$i:$v") }
                    println("|${thatLane.size}|")

                } else {
                    print("p:X l:X->")
                    state.players[playerId].rows[l].forEachIndexed { _, v -> print("|X||$v|") }
                    println("|X|")
                }
            }
        }
        return readVal("Pick spot ", { it.toCardLocation() }) {
            mask.check(playerId, it) && it.positionId in 0..state.players[it.playerId].rows[it.rowId].size
        }
    }
    override suspend fun printMess(printOption: State.PrintOption, card: Card?) {
        println("$printOption by $card")
    }


}

fun String.toCardLocation(): CardLocation {
    val x = split(" ").map { it.toInt() }
    return CardLocation(x[0], x[1], x[2])

}
fun readFile(deck: File): List<String>{
    TODO()
}