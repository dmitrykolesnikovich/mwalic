package walic.jvmWalic


import walic.pureEngine.*

import java.awt.*
import java.io.File
import java.io.FileWriter
import java.time.LocalDateTime
import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter
import kotlin.random.Random

class DeckCreator {
    val searchFile = JTextField(16)
    private val cardsPanel = JPanel()
    private val deckPanel = JPanel()
    private val betterAll: List<Pair<AbstractCard, List<String>>> = allNormalCards.sortedWith(compareBy(AbstractCard::getMaxInInnitialDeck, AbstractCard::name)).map { Pair(it, it.asFanyText()) }
    private val frame = JFrame("Deck Creator")
    private val leaderBox = JComboBox(allLeaders.toTypedArray())

    private val statusBox = JLabel()
    var currentDeck = mutableListOf<AbstractCard>()


    private fun doSearch() {
        cardsPanel.removeAll()
        val split = searchFile.text.split(" ")
        val fraction = (leaderBox.selectedItem as AbstractCard).fraction!!
        betterAll.filter { pair -> (pair.first.ist(fraction) || pair.first.fraction == null) && (split.isEmpty() || split.all { pair.second.joinToString().contains(it, ignoreCase = true) }) }.forEach { pair ->
            val singleCardPanel = JPanel()
            val buttonPanel = JPanel()
            val button = JButton("Add 1")
            singleCardPanel.layout = GridLayout(0, 1)
            button.addActionListener {
                currentDeck.add(pair.first)
                printDeck()
            }
            for (t in pair.second)
                singleCardPanel.add(JLabel(t))
            singleCardPanel.border = BorderFactory.createLineBorder(Color.BLACK)
            buttonPanel.add(button, BorderLayout.WEST)
            singleCardPanel.add(buttonPanel)
            cardsPanel.add(singleCardPanel)
        }

        frame.invalidate()
        frame.validate()
        frame.repaint()
    }

    private fun printDeck() {
        deckPanel.removeAll()
        currentDeck = currentDeck.sortedWith(compareBy(AbstractCard::getMaxInInnitialDeck, AbstractCard::name)).toMutableList()
        (listOf(leaderBox.selectedItem as AbstractCard) + currentDeck).groupBy { it.name }.forEach { (_, list) ->
            val singleCardPanel = JPanel()
            val buttonPanel = JPanel()
            val button = JButton("Remove 1")
            singleCardPanel.layout = GridLayout(0, 1)
            button.addActionListener {
                currentDeck.remove(list[0])
                printDeck()
            }
            for (t in list[0].asFanyText())
                singleCardPanel.add(JLabel(t))
            singleCardPanel.border = BorderFactory.createLineBorder(Color.BLACK)
            buttonPanel.add(button, BorderLayout.WEST)
            buttonPanel.add(JLabel("Count = ${list.size} / ${list[0].getMaxInInnitialDeck()}$"), BorderLayout.WEST)
            singleCardPanel.add(buttonPanel)
            deckPanel.add(singleCardPanel)
        }
        try {
            checkIfDeckOk(leaderBox.selectedItem as AbstractCard, currentDeck)
            statusBox.text = "OK"
        } catch (e: WrongInputException) {
            statusBox.text = e.message
        }
        statusBox.text += " Gold ${currentDeck.count { it.ist(Tag.Gold) }} / $maxGold , Silver  ${currentDeck.count { it.ist(Tag.Silver) }} / $maxSilver"
        frame.invalidate()
        frame.validate()
        frame.repaint()
    }

    init {
        UIManager.put("Label.font", Font("Serif", Font.ITALIC, 16))
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame.extendedState = JFrame.MAXIMIZED_BOTH
        frame.preferredSize = Dimension(1600, 800)
        val cardControlPanel = JPanel()
        cardControlPanel.add(searchFile)
        searchFile.addActionListener {
            doSearch()
        }
        leaderBox.addActionListener {
            doSearch()
            printDeck()
        }
        doSearch()
        printDeck()
        val saveButton = JButton("Save")
        saveButton.addActionListener {
            val chooser = JFileChooser("Decks/")
            val filter = FileNameExtensionFilter("Gwent Decks", "wcd")
            chooser.fileFilter = filter
            chooser.selectedFile = File("${(leaderBox.selectedItem as Card).ac.getNiceName().replace(" ", "")}-${Random.nextInt()}.wcd")
            chooser.showSaveDialog(frame)
            val writer = FileWriter(chooser.selectedFile)
            writer.write("# deck created @ ${LocalDateTime.now()}\n")
            writer.write((leaderBox.selectedItem as Card).name + "\n")
            for (card in currentDeck) {
                writer.write(card.name + "\n")
            }
            writer.close()
        }
        cardControlPanel.add(saveButton)
        val loadButton = JButton("Load")
        loadButton.addActionListener {
            val chooser = JFileChooser("Decks/")
            val filter = FileNameExtensionFilter("Gwent Decks", "wcd")
            chooser.fileFilter = filter
            chooser.showOpenDialog(frame)
            val rf = readFile(chooser.selectedFile)
            val pair = praseDeck(rf) { 0 }
            leaderBox.selectedIndex = allLeaders.indexOfFirst { pair.first.name == it.name }
            currentDeck = pair.second.map { it.ac }.toMutableList()
            printDeck()
        }
        cardControlPanel.add(loadButton)
        cardControlPanel.add(statusBox)
        cardControlPanel.add(leaderBox)

        cardsPanel.layout = GridLayout(0, 2)
        val cardPanelScrol = JScrollPane(cardsPanel)
        cardPanelScrol.verticalScrollBar.unitIncrement = 16
        frame.contentPane.add(BorderLayout.WEST, cardPanelScrol)

        deckPanel.layout = GridLayout(0, 2)
        val deckPanelScrol = JScrollPane(deckPanel)
        deckPanelScrol.verticalScrollBar.unitIncrement = 16
        frame.contentPane.add(BorderLayout.EAST, deckPanelScrol)
        frame.contentPane.add(BorderLayout.NORTH, cardControlPanel)
        frame.isVisible = true
    }
}
