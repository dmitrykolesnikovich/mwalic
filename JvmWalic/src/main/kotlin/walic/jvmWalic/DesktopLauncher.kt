package walic.jvmWalic

import kotlinx.coroutines.*
import walic.pureEngine.*
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.File
import java.lang.Thread.sleep
import java.net.ServerSocket
import java.net.Socket
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.random.Random


fun getSocket(hostname: String, port: Int): Socket {
    var count = 0
    while (true) {
        try {
            return Socket(hostname, port)
        } catch (e: java.lang.Exception) {
            println("Attempt no ${count++} failed with $e")
            sleep(1000)
        }
    }

}

@Volatile
var crashesLoged = 0
var interCounter = AtomicInteger()

fun runTest(threads: Int) {
    (0 until threads).forEach { tId ->
        thread {
            while (true) {
                val streams = listOf(ArrayBlockingQueue<String>(10), ArrayBlockingQueue<String>(10))
                val time = System.currentTimeMillis()
                listOf(0, 1).map { p ->
                    (0..Int.MAX_VALUE).forEach { inter ->
                        val randomSeed = Random.nextLong()
                        val adapter = RadnomAdapter(true, Random(randomSeed))
                        val steamAdapter = SteamAdapter(streams[p]::take, streams[1 - p]::put, p, adapter, debugLevel = 10)
                        try {
                            runBlocking {
                                State(steamAdapter).mainLoop()
                            }
                        } catch (e: Exception) {
                            println("Random seeed was $randomSeed for $p")
                            adapter.log.forEach { println("$p -> $it") }
                            val file = File("replies/crushDump$p-$inter.wrp")
                            file.writeText(steamAdapter.getReplay())
                            streams[1 - p].put("Some random value that will crash that other stream Xd no the best idea but whatever")
                            streams[1 - p].put("}[} one more time to be sure")
                            throw e
                        }
                        if (p == 0) {
                            interCounter.getAndAdd(1)
                        }
                        if (p == 0 && tId == 0 && inter % 10 == 0)
                            println("Iter $inter per sec= ${inter.toFloat() / (System.currentTimeMillis() - time) * 1000f} (${interCounter.toFloat() / (System.currentTimeMillis() - time) * 1000f}) , crashesLoged $crashesLoged")
                    }

                }
                crashesLoged++
            }
        }
    }
}


suspend fun playReplay(replayPath: String?) {
    val file = File(
        if (replayPath != null) replayPath else {
            println("Enter filename");"replies/" + readLine()!!
        }
    )
    val replay = creatReplyFromString(file.readText())
    val adapter = replay.getStreamAdapter()
    State(adapter, false).mainLoop()
}

object DesktopLauncher {
    private const val port = 12345

    @JvmStatic
    fun main(args: Array<String>) {
        val mod: String = if (args.isEmpty()) {
            println("enter deck|cmd|gui|rng|rep")
            readLine()!!
        } else {
            args[0]
        }

        val deck = args.elementAtOrNull(2)

        val playerAdapter: PlayerAdapter = when (mod) {
            "deck" -> {
                DeckCreator();return
            }
            "rng" -> {
                println("Enter seeds")
                val seeds = readLine()!!.split(" ").map { it.toLong() }
                val streams = listOf(ArrayBlockingQueue<String>(10), ArrayBlockingQueue<String>(10))
                listOf(0, 1).map { p ->
                    thread {
                        val adapter = RadnomAdapter(p == 0, Random(seeds[p]))
                        val steamAdapter = SteamAdapter(streams[p]::take, streams[1 - p]::put, p, adapter,0)
                        runBlocking {
                            State(steamAdapter).mainLoop()
                        }
                    }
                }.forEach { it.join() }
                return
            }
            "cmd" -> ConsolePlayerAdapter(deck)
//            "gui" -> GuiAdapter(runSecondGui(), deck)
            "rep" -> {
                runBlocking {
                    playReplay(deck)
                }
                return
            }
            "test" -> {
                runTest(deck!!.toInt());return
            }
            else -> throw IllegalArgumentException()
        }
        val hostname = if (args.size < 2) {
            println("enter hostname or null or auto")
            readLine()!!
        } else {
            args[1]
        }
        val playerId: Int
        val inSteam: () -> String
        val outSteam: (String) -> Unit
        //TODO supend bullshit
//        when (hostname) {
//            "null" -> {
//                println("Starting as server at port:$port")
//                val socket = ServerSocket(port).accept()
//                inSteam = DataInputStream(socket.getInputStream())::readUTF
//                outSteam = DataOutputStream(socket.getOutputStream())::writeUTF
//                playerId = 0
//            }
//            "auto" -> {
//                val streams = listOf(ArrayBlockingQueue<String>(10), ArrayBlockingQueue<String>(10))
//                inSteam = streams[0]::take
//                outSteam = streams[1]::put
//                playerId = 0
//                thread {
//                    State(SteamAdapter(streams[1]::take, streams[0]::put, 1, RadnomAdapter(true, Random))).mainLoop()
//                }
//            }
//            else -> {
//                println("Starting with host:$hostname and port:$port")
//                val socket = getSocket(hostname, port)
//                inSteam = DataInputStream(socket.getInputStream())::readUTF
//                outSteam = DataOutputStream(socket.getOutputStream())::writeUTF
//
//                playerId = 1
//            }
//        }

//        val steamAdapter = SteamAdapter(inSteam, outSteam, playerId, playerAdapter)
//        State(steamAdapter).mainLoop()
//        println("Write to file ")
//        val read = readLine()!!
//        if (read == "")
//            return
//        File(read).writeText(steamAdapter.getReplay())


    }
}




